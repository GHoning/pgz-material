# We importen drie dingen uit pgzero: de Actor klasse, het keyboard en sounds.
from pgzero.builtins import Actor, keyboard, sounds
from projectile import Projectile

# Dit is hoe we onze klasse beginnen.
class Spaceship(Actor):
    # Dit is de init functie, deze wordt als aller eerst aangeroepen zodra er een nieuwe instantie aangemaakt wordt. Voor het starten van een ruimteschip instantie hebben we drie dingen nodig: een plaatje, de hoogte en breedte van het speelveld.
    def __init__(self, image, SCREENWIDTH, SCREENHEIGHT):
        # Hiermee vertelen we dat de Actor zichzelf ook mag gaan starten, dit zodat we gebruik kunnen maken de eigenschappen van de Actor.
        Actor.__init__(self,image)
        # Dit zijn de variabelen die we al hadden voor ons ruimteschip. Er is wel een ding veranderd. We zeggen nu self.<variabele naam> hier zeggen we dat het een eigenschap is van zichzelf, het ruimteschip dus.
        self.MAX_X_SPEED = 15
        self.MAX_Y_SPEED = 15
        self.X_ACCELERATION = 1
        self.Y_ACCELERATION = 1
        self.AIR_RESISTANCE = 0.5
        self.BLOCKED_UP = False
        self.BLOCKED_DOWN = False
        self.BLOCKED_RIGHT = False
        self.BLOCKED_LEFT = False
        self.SCREENWIDTH = SCREENWIDTH
        self.SCREENHEIGHT = SCREENHEIGHT
        self.x = 500
        self.y = 800
        self.x_speed = 0
        self.y_speed = 0

        self.PROJECTILE_SPEED = 16
        self.fired = False

    # We maken een aparte update loop voor ons ruimteschip.
    def update(self, projectiles):
        # Als we spatiebalk indrukken schieten we een nieuw projectiel.
        if keyboard.space:
            if not self.fired:
                self.fired = True
                new_projectile = Projectile('projectile')
                new_projectile.x = self.x
                new_projectile.y = self.y - 30
                projectiles.append(new_projectile)
                sounds.laser.play()
        else:
            self.fired = False

        # Als we het pijltje naar rechts indrukken en we worden niet tegen gehouden:
        if keyboard.right and not self.BLOCKED_RIGHT:
            # Dan kunnen we weer naar links bewegen.
            self.BLOCKED_LEFT = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.x_speed < self.MAX_X_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.x_speed += self.X_ACCELERATION
        # Als we het pijltje naar links indrukken en we worden niet tegen gehouden:
        elif keyboard.left and not self.BLOCKED_LEFT:
            # Dan kunnen we weer naar rechts bewegen.
            self.BLOCKED_RIGHT = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.x_speed > -self.MAX_X_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.x_speed -= self.X_ACCELERATION
        # Als we niet rechts of links indrukken
        else:
            # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
            if self.x_speed < -1:
                self.x_speed += self.AIR_RESISTANCE
            elif self.x_speed > 1:
                self.x_speed -= self.AIR_RESISTANCE
            else:
                self.x_speed = 0

        # Als het ruimteschip tegen de rechterkant van het scherm aankomt:
        if self.x > self.SCREENWIDTH:
            # Dan stoppen we het ruimteschip daar en blokeren we naar rechts gaan.
            self.x = self.SCREENWIDTH
            self.x_speed = 0
            self.BLOCKED_RIGHT = True
        # Als het ruimteschip tegen de linkerkant van het scherm aankomt:
        elif self.x < 0:
            # Dan stoppen we het ruimteschip daar en blokeren we naar links gaan.
            self.x = 0
            self.x_speed = 0
            self.BLOCKED_LEFT = True
        # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
        else:
            self.x += self.x_speed

        # Als we pijltje omhoog indrukken en we worden niet tegen gehouden:
        if keyboard.up and not self.BLOCKED_UP:
            # Dan kunnen we weer naar onderen bewegen.
            self.BLOCKED_DOWN = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.y_speed < self.MAX_Y_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.y_speed += self.Y_ACCELERATION
        # Als we pijltje naar beneden indrukken en we worden niet tegen gehouden:
        elif keyboard.down and not self.BLOCKED_DOWN:
            # Dan kunnen we weer naar boven bewegen.
            self.BLOCKED_UP = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.y_speed > -self.MAX_Y_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.y_speed -= self.Y_ACCELERATION
        # Als we niet naar boven of naar onderen indrukken
        else:
            # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
            if self.y_speed < -1:
                self.y_speed += self.AIR_RESISTANCE
            elif self.y_speed > 1:
                self.y_speed -= self.AIR_RESISTANCE
            else:
                self.y_speed = 0

        # Als het ruimteschip tegen de onderkant van het scherm aankomt:
        if self.y > self.SCREENHEIGHT:
            # Dan stoppen we het ruimteschip daar en blokeren we naar beneden gaan.
            self.y = self.SCREENHEIGHT
            self.y_speed = 0
            self.BLOCKED_DOWN = True
        # Als het ruimteschip tegen de bovenkant van het speelveld aankomt (we nemen niet het hele scherm):
        elif self.y < 400:
            # Dan stoppen we het ruimteschip daar en blokeren we naar boven gaan.
            self.y = 400
            self.y_speed = 0
            self.BLOCKED_UP = True
        # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
        else:
            self.y -= self.y_speed