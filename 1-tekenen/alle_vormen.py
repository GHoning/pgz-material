WIDTH = 800
HEIGHT = 600

# Hier bepalen we de kleur die het vierkant gaat krijgen.
RED = 200, 0, 0

# Dit is de doos die we gaan tekenen zo meteen. Het eerste deel is de positie van de doos.
# Het tweede deel is de breedte en de hoogte van de doos.
BOX = Rect((20, 20), (100, 100))

# Deze kleur maken we onze cirkel.
GREEN = 0, 200, 0

# Dit is de positie van het midden van de cirkel.
POS = 100, 100

# De radius van een cirkel is de afstand van af het midden tot de rand van de cirkel
RADIUS = 50

# De kleur die onze lijn gaat worden.
BLUE = 0, 0, 200

# Het start coördinaat van de lijn.
START = 100, 100

# Het eind coördinaat van de lijn.
END = 700, 500


# Dit is een functie, dit is een verzameling van instructies voor de computer.
# In dit geval hebben we een functie die "draw" heet, Pygame-Zero verwacht deze functie. (Daarom heeft hij een Engelse naam.)
# Hier bepalen wij wat we gaan tekenen.
def draw():
  # Wij tekenen ons vierkant in het rood.
  screen.draw.filled_rect(BOX, RED)

  # Dit is hoe we een cirkel tekenen.
  screen.draw.filled_circle(POS, RADIUS, GREEN)

  # Nu tekenen we de lijn
  screen.draw.line(START, END, BLUE)
