WIDTH = 330
HEIGHT = 290

# We creëren de knoppen voor ons soundboard
cow = Actor('cow', (85, 75))
duck = Actor('duck', (245, 75))
pig = Actor('pig', (85, 215))
sheep = Actor('sheep', (245, 215))

# We tekenen de knoppen
def draw():
    cow.draw()
    duck.draw()
    pig.draw()
    sheep.draw()

# Hier checken we of er op de knoppen klikt is en zo ja spelen we het geluid af
def on_mouse_down(pos):
  if cow.collidepoint(pos):
    sounds.cow.play()
  elif duck.collidepoint(pos):
    sounds.duck.play()
  elif pig.collidepoint(pos):
    sounds.pig.play()
  elif sheep.collidepoint(pos):
    sounds.sheep.play()
