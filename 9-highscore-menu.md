## Menu and highscores
Nu gaan we aanpassingen maken aan onze game, we gaan een menu toevoegen voordat ons spel begint. Daarnaast gaan we een highscore systeem toevoegen zodat de spelers een highscore neer kunnen zetten en opslaan.

### Button
We beginnen met het maken van een nieuwe klasse die ```Button``` heet. Dit wordt een knop die we vaker kunnen gebruiken. Maak een nieuw bestand aan genaamd ```button.py``` en zet hier de volgende code in.
```python
from pgzero.rect import Rect
import pygame.mouse as mouse

# De button klasse
class Button():
    # De variabelen van de de button klasse.
    box = None
    color = None
    name = None
    textPos = None
    function = None

    def __init__(self, name, pos, size, color, function, **kwargs):
        self.name = name
        self.box = Rect(pos, size)
        self.color = color
        self.textPos = (pos[0] + size[0]/2, pos[1] + size[1]/2)
        # We gaan eens iets nieuws doen, we geven een functie mee.
        self.function = function

    def draw(self, screen):
        screen.draw.filled_rect(self.box, self.color)
        screen.draw.text(
            str(self.name),
            color='white',
            center=self.textPos,
            fontsize=35,
            shadow=(1, 1)
        )

    def update(self):
        # Als linkermuisknop ingedrukt wordt en de muis cursor bevindt zich binnen de knop voer de knop functie uit.
        if mouse.get_pressed()[0] and self.box.collidepoint(mouse.get_pos()):
            self.function()
```

### Menu
Nu we de knop hebben kunnen we een menu maken voor ons spel, maak een nieuw bestand aan genaamd ```tyrian_menu.py``` en zet hier de volgende code in. Hiermee controleren we of de knoppen kunnen tekenen, dus nog niet of we er op kunnen klikken. Dat komt in het volgende stuk.
```python
#from tyrian_game import TyrianGame
from button import Button

# Game variablen.
TITLE = 'tyrian++'
WIDTH = 900
HEIGHT = 1000

# De functie die we gaan gebruiken om de game te starten.
def start_game():
    pass

# De functie die we gaan gebruiken om de game mee te stoppen.
def stop_game():
    pass

# De functie die we gaan gebruiken om het highscore menu te laten zien.
def show_highscore():
    pass

# De functie die we gaan gebruiken om het highscore menu te verstoppen.
def hide_highscore():
    pass


startButton = Button("Start",(WIDTH/2 - 100, 100),(200, 100), "grey",start_game)
highscoreButton = Button("Highscore",(WIDTH/2 - 100, 300),(200, 100), "grey", show_highscore)
backButton = Button("Back",(WIDTH/2 - 100, 600),(200, 100), "grey", hide_highscore)

def update():
    pass

def draw():
    screen.clear()
    startButton.draw(screen)
    highscoreButton.draw(screen)
```

### Starting the game
We gaan het nu zo maken dat we het spel kunnen starten met de start game knop. Maak een nieuw bestand aan met de naam ```tyrian_game.py``` en zet daar de volgende code in.
```python
from pgzero.builtins import keyboard, sounds, clock, keys
from spaceship import Spaceship
from projectile import Projectile
from scrolling_background import ScrollingBackground
from asteroid import Asteroid
import random

class TyrianGame():

    def __init__(self, width, height, stop):
        self.stop = stop
        self.game_over = False
        self.score = 0
        self.width = width
        self.height = height
        # Ons ruimteschip met al zijn variablen.
        self.spaceship = Spaceship('spaceship', width, height)

        # Onze achtergronden.
        self.background = ScrollingBackground('space_background')

        # Asteroïden
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.asteroids = []

        # Projectielen
        self.projectiles = []


    def spawn_asteroids(self):
        # Als de game nog niet voor bij:
        if not self.game_over:
            # Dan spawnen we een nieuwe asteroïde in.
            new_asteroid = Asteroid('asteroid', self.height)
            new_asteroid.x = random.randint(0, self.width)
            new_asteroid.y = 0
            new_asteroid.speed = self.asteroid_speed
            # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
            self.asteroids.append(new_asteroid)
            # We zetten een timer voor het spawnen van een nieuwe asteroïde.
            clock.schedule_unique(self.spawn_asteroids, self.asteroid_spawn_rate)

    def start_game(self):
        self.score = 0
        self.asteroids = []
        self.spaceship.projectiles = []
        self.game_over = False
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.spaceship.image = "spaceship"
        self.spawn_asteroids()
        self.spaceship.x = self.width/2
        self.spaceship.y = self.height/2 + 400


    def check_for_collisions(self):
        # Voor asteroïde in asteroïden
        for asteroid in self.asteroids:
            if asteroid is not None:
                # Als asteroïde het ruimteschip raakt:
                if asteroid.colliderect(self.spaceship):
                    # Is het spel over en explodeert het ruimteschip.
                    self.game_over = True
                    sounds.explosion.play()
                    self.spaceship.image = "explosion"

                # Voor projectiel in de lijst van projectielen.
                for projectile in self.projectiles:
                    if projectile is not None:
                        # Als asteroïde geraakt wordt door het projectiel.
                        if asteroid.colliderect(projectile):
                            # Spelen we een geluid af
                            sounds.explosion.play()
                            # Halen we de asteroïde en het projectiel uit de lijst.
                            self.asteroids.remove(asteroid)
                            self.projectiles.remove(projectile)
                            # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                            self.score += 1
                            self.asteroid_speed += 0.25
                            if self.asteroid_spawn_rate > 0.25:
                                self.asteroid_spawn_rate -= 0.25


    def update(self):
        # Als de game nog niet voor bij is updaten we ons ruimteschip en andere spel elementen.
        if not self.game_over:
            self.spaceship.update(self.projectiles)
            self.background.update()

            # Voor elke projectiel in de lijst van projectielen:
            for projectile in self.projectiles:
                projectile.update(self.projectiles)

            # Voor elke asteroide in de lijst van asteroiden:
            for asteroid in self.asteroids:
                asteroid.update(self.asteroids)

            self.check_for_collisions()

        if keyboard[keys.RETURN] and self.game_over:
            self.stop()


    def draw(self, screen):
        # We tekenen de achtergrond en het ruimteschip.
        screen.clear()
        self.background.draw()
        self.spaceship.draw()
        for asteroid in self.asteroids:
            asteroid.draw()

        for projectile in self.projectiles:
            projectile.draw()

        screen.draw.text(
            str(self.score),
            color='white',
            midtop =(self.width/2, 10),
            fontsize=70,
        )

        if self.game_over:
            screen.draw.text(
                "Game Over\nPress \"Enter\" to quit",
                color='white',
                midtop =(self.width/2, self.height/2),
                fontsize=70,
            )
```
Dan moeten we ons menu nog aanpassen naar het volgende:
```python
from tyrian_game_menu import TyrianGame
from button import Button

# Game variablen.
TITLE = 'tyrian++'
WIDTH = 900
HEIGHT = 1000

# De functie die we gaan gebruiken om de game te starten.
def start_game():
    global runningGame
    runningGame = True
    game.start_game()

# De functie die we gaan gebruiken om de game mee te stoppen.
def stop_game():
    global runningGame
    runningGame = False

# De functie die we gaan gebruiken om het highscore menu te laten zien.
def show_highscore():
    pass

# De functie die we gaan gebruiken om het highscore menu te verstoppen.
def hide_highscore():
    pass


startButton = Button("Start",(WIDTH/2 - 100, 100),(200, 100), "grey",start_game)
highscoreButton = Button("Highscore",(WIDTH/2 - 100, 300),(200, 100), "grey", show_highscore)
backButton = Button("Back",(WIDTH/2 - 100, 600),(200, 100), "grey", hide_highscore)

game = TyrianGame(WIDTH, HEIGHT, stop_game)
runningGame = False


def update():
    if runningGame:
        game.update()
    else:
        startButton.update()
        highscoreButton.update()


def draw():
    screen.clear()
    if runningGame:
        game.draw(screen)
    else:
        startButton.draw(screen)
        highscoreButton.draw(screen)
```

### Highscore laten zien
Om de highscores te laten zien moeten de de menu code aanpassen en hebben we een bestand nodig waar de highscores in opgeslagen zijn. We gaan het bestand uitlezen door in de code eerst het bestand te open, te lezen wat we nodig hebben en daarna het bestand weer te sluiten. Met ```open()``` maken we het bestand open. Met ```readlines()``` lezen we de regels van het bestand daarna sluiten we het bestand met ```close()```. Om het highscore menu te testen kun je dit [bestand](9-highscore-menu/highscores.txt) gebruiken zet dit bestand in dezelfde folder als je code. Pas ```tyrian_menu.py``` als volgt aan.
```python
from tyrian_game import TyrianGame
from button import Button

# Game variablen.
TITLE = 'tyrian++'
WIDTH = 900
HEIGHT = 1000

# De functie die we gaan gebruiken om de game te starten.
def start_game():
    global runningGame
    runningGame = True
    game.start_game()

# De functie die we gaan gebruiken om de game mee te stoppen.
def stop_game():
    global runningGame
    runningGame = False

# De functie die we gaan gebruiken om het highscore menu te laten zien.
def show_highscore():
    global showingHighscore
    showingHighscore = True

# De functie die we gaan gebruiken om het highscore menu te verstoppen.
def hide_highscore():
    global showingHighscore
    showingHighscore = False


startButton = Button("Start",(WIDTH/2 - 100, 100),(200, 100), "grey",start_game)
highscoreButton = Button("Highscore",(WIDTH/2 - 100, 300),(200, 100), "grey", show_highscore)
backButton = Button("Back",(WIDTH/2 - 100, 600),(200, 100), "grey", hide_highscore)

game = TyrianGame(WIDTH, HEIGHT, stop_game)
runningGame = False
showingHighscore = False

def update():
    if runningGame:
        game.update()
    elif showingHighscore:
        backButton.update()
    else:
        startButton.update()
        highscoreButton.update()


def draw():
    screen.clear()
    if runningGame:
        game.draw(screen)
    elif showingHighscore:
        # We openen het highscore bestand.
        highscore_file = open("highscores.txt", "r")
        # We lezen de regels die we nodig hebben.
        lines = highscore_file.readlines()
        # We sluiten de file weer.
        highscore_file.close()
        # We tekenen de scores die we uit het betand gehaald hebben.
        for index, line in enumerate(lines):
            screen.draw.text(
                str(line),
                color='white',
                midtop =(WIDTH/2, 100 + 40 * index),
                fontsize=40,
            )
        backButton.draw(screen)
    else:
        startButton.draw(screen)
        highscoreButton.draw(screen)
```

### Highscore scoren en opslaan
Om een highscore te kunnen zetten moet we een score wegschrijven naar het highscore bestand. Dit doen we wanneer de speler een score heeft gehaald die hoger is dan de laagste score in het highscore bestand. Hiervoor gaan we een nieuwe klasse aanmaken die de speler kan gebruiken om een afkorting van zijn of haar naam in te vullen. Maak een nieuw bestand aan genaamd ```highscore_widget.py``` en zet daar de volgende code in.
```python
from pgzero.builtins import keyboard, keys

class HighscoreWidget():
    def __init__(self, pos, score, stop, **kwargs):
        # Variabelen en constanten die gebruikt worden door de highscore widget.
        self.ALPHABET = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
        self.stop = stop
        self.positions = [pos, (pos[0] +50, pos[1]), (pos[0] +100, pos[1])]
        self.text_pos = (pos[0]+ 50, pos[1] - 100)
        self.score = score
        self.characters = [0, 0, 0]
        self.selected = 0
        self.pressed = False

    def update(self):
        # Hiermee scrollen we door de widget heen.
        if keyboard.up and not self.pressed:
            self.pressed = True
            if self.characters[self.selected] > 24:
                self.characters[self.selected] = 0
            else:
                self.characters[self.selected] += 1
        elif keyboard.down and not self.pressed:
            self.pressed = True
            if self.characters[self.selected] < 1:
                self.characters[self.selected] = 25
            else:
                self.characters[self.selected] -= 1
        elif keyboard.left and not self.pressed:
            self.pressed = True
            if self.selected > 0:
                self.selected -= 1
        elif keyboard.right and not self.pressed:
            self.pressed = True
            if self.selected < 2:
                self.selected += 1
        # Hier accepteren we de naam van de speler en schrijven we de behaalde highscore weg.
        elif keyboard[keys.RETURN] and not self.pressed:
            self.pressed = True
            # We openen het highscore bestand.
            file = open("highscores.txt", "r")
            # We lezen de inhoud van het bestand.
            lines = file.readlines()
            # We sluiten de file weer.
            file.close()
            # Hier pluizen we de inhoud van het highscore bestand uit elkaar in de namen en scores van de spelers.
            names = []
            scores = []
            for line in lines:
                result = line.split("...")
                names.append(result[0])
                scores.append(int(result[1]))

            # We voegen de door de huidige speler behaalde score toe.
            scores.append(self.score)
            # We sorteren de scores van hoog naar laag.
            scores.sort(reverse=True)
            # We filteren de namen zodat ze weer bij de juiste score staan.
            names.insert(scores.index(self.score) , self.ALPHABET[self.characters[0]] + self.ALPHABET[self.characters[1]] + self.ALPHABET[self.characters[2]])
            # Nu hebben we elf namen met een score we willen er maar tien onthouden dus we knippen er een af.
            names = names[0:10]

            # We openen het highscore betand weer.
            file = open("highscores.txt", "w")
            # We schrijven de naam en score van de spelers weer in het highscore bestand.
            for index, name in enumerate(names):
                file.write(name + "..." + str(scores[index]) + "\n")
            # Na het schrijven sluiten we het highscore bestand weer.
            file.close()
            # Close to game from here.
            self.stop()
        elif not keyboard.up and not keyboard.down and not keyboard.left and not keyboard.right and not keyboard[keys.RETURN]:
            self.pressed = False


    def draw(self, screen):
        screen.draw.text(
            "Congratulations you got a highscore!\n use the arrow keys to select a name\n press \"enter\" to save your highscore",
            color='white',
            center=self.text_pos,
            fontsize=30,
            shadow=(1, 1)
        )

        for index, char in enumerate(self.characters):
            if index == self.selected:
                screen.draw.text(
                    str(self.ALPHABET[char]),
                    color='white',
                    center=self.positions[index],
                    fontsize=45,
                    shadow=(1, 1)
                )
            else:
                screen.draw.text(
                    str(self.ALPHABET[char]),
                    color='white',
                    center=self.positions[index],
                    fontsize=30,
                    shadow=(1, 1)
                )
```
Nu moeten we de ```tyrian_game.py``` nog aanpassen naar het volgende om de highscore widget te tekenen.
```python
from pgzero.builtins import keyboard, sounds, clock, keys
from spaceship import Spaceship
from projectile import Projectile
from scrolling_background import ScrollingBackground
from asteroid import Asteroid
from highscore_widget import HighscoreWidget
import random

class TyrianGame():

    def __init__(self, width, height, stop):
        self.stop = stop
        self.game_over = False
        self.score = 0
        self.width = width
        self.height = height
        # Ons ruimteschip met al zijn variablen.
        self.spaceship = Spaceship('spaceship', width, height)

        # Onze achtergronden.
        self.background = ScrollingBackground('space_background')

        # Asteroïden
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.asteroids = []

        # Projectielen
        self.projectiles = []

        self.widget = None
        self.new_high = False

    def spawn_asteroids(self):
        # Als de game nog niet voor bij:
        if not self.game_over:
            # Dan spawnen we een nieuwe asteroïde in.
            new_asteroid = Asteroid('asteroid', self.height)
            new_asteroid.x = random.randint(0, self.width)
            new_asteroid.y = 0
            new_asteroid.speed = self.asteroid_speed
            # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
            self.asteroids.append(new_asteroid)
            # We zetten een timer voor het spawnen van een nieuwe asteroïde.
            clock.schedule_unique(self.spawn_asteroids, self.asteroid_spawn_rate)

    def start_game(self):
        self.score = 0
        self.asteroids = []
        self.spaceship.projectiles = []
        self.game_over = False
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.spaceship.image = "spaceship"
        self.spawn_asteroids()
        self.spaceship.x = self.width/2
        self.spaceship.y = self.height/2 + 400

    def new_highscore(self, score):
        highscore_file = open("highscores.txt", "r")
        last_line = highscore_file.readlines()[9]
        highscore_file.close()
        old_score = int(last_line.split("...")[1])

        if score > old_score:
            return True
        else:
            return False


    def check_for_collisions(self):
        # Voor asteroïde in asteroïden
        for asteroid in self.asteroids:
            if asteroid is not None:
                # Als asteroïde het ruimteschip raakt:
                if asteroid.colliderect(self.spaceship):
                    # Is het spel over en explodeert het ruimteschip.
                    self.game_over = True
                    sounds.explosion.play()
                    self.spaceship.image = "explosion"
                    if self.new_highscore(self.score):
                        # Spawn the highscore widget. If the score succeeds the smallest stored score.
                        self.new_high = True
                        self.widget = HighscoreWidget((self.width/2,600), self.score, self.stop)


                # Voor projectiel in de lijst van projectielen.
                for projectile in self.projectiles:
                    if projectile is not None:
                        # Als asteroïde geraakt wordt door het projectiel.
                        if asteroid.colliderect(projectile):
                            # Spelen we een geluid af
                            sounds.explosion.play()
                            # Halen we de asteroïde en het projectiel uit de lijst.
                            self.asteroids.remove(asteroid)
                            self.projectiles.remove(projectile)
                            # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                            self.score += 1
                            self.asteroid_speed += 0.25
                            if self.asteroid_spawn_rate > 0.25:
                                self.asteroid_spawn_rate -= 0.25


    def update(self):
        # Als de game nog niet voor bij is updaten we ons ruimteschip en andere spel elementen.
        if not self.game_over:
            self.spaceship.update(self.projectiles)
            self.background.update()

            # Voor elke projectiel in de lijst van projectielen:
            for projectile in self.projectiles:
                projectile.update(self.projectiles)

            # Voor elke asteroide in de lijst van asteroiden:
            for asteroid in self.asteroids:
                asteroid.update(self.asteroids)

            self.check_for_collisions()
        elif self.widget is not None:
            self.widget.update()

        if keyboard[keys.RETURN] and self.game_over and not self.new_high:
            self.stop()


    def draw(self, screen):
        # We tekenen de achtergrond en het ruimteschip.
        screen.clear()
        self.background.draw()
        self.spaceship.draw()
        for asteroid in self.asteroids:
            asteroid.draw()

        for projectile in self.projectiles:
            projectile.draw()

        screen.draw.text(
            str(self.score),
            color='white',
            midtop =(self.width/2, 10),
            fontsize=70,
        )

        if self.game_over and self.widget is not None and self.new_high:
            self.widget.draw(screen)
        elif self.game_over and not self.new_high:
            screen.draw.text(
                "Game Over\nPress \"Enter\" to quit",
                color='white',
                midtop =(self.width/2, self.height/2),
                fontsize=70,
            )
```

### Complete Code
De complete code kan [hier](9-highscore-menu/tyrian_highscore.zip) gedownload worden.

[<- Terug](8-tyrian-verbeteringen.md) - [Volgende ->](10-einde.md)

##### Extra uitdaging
Voeg een stop knop toe aan het menu zodra we op deze knop klikken moet het spel zich afsluiten. Hiervoor zul je ```sys.exit()``` moeten gebruiken voor we dat kunnen gebruiken moeten we ```sys``` wel importeren. 

<details><summary>Hint</summary>
Maak een functie genaamd ```quit game``` en maak een knop waarmee je deze aanroept.
</details>

#### Credits
De achtergrond is gemaakt door [Marja]( https://www.instagram.com/marjavdhoning/). Het plaatje voor het ruimteschip, het projectiel en de explosie zijn gemaakt door [CDmir](https://opengameart.org/users/cdmir) en komen uit zijn [asteroids-game-sprites-atlas](https://opengameart.org/content/asteroids-game-sprites-atlas). Het geluid voor de laser komen van [laser-fire](https://opengameart.org/content/laser-fire) en zijn gemaakt door [dklon](https://opengameart.org/users/dklon). Het geluid voor de explosie komt van [explosion-0](https://opengameart.org/content/explosion-0) en is gemaakt door [TinyWorlds](https://opengameart.org/users/tinyworlds).
