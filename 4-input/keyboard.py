WIDTH = 1024
HEIGHT = 1024

background = Actor('background_move')
alien1 = Actor('alien1', (512, 512))

def draw():
    background.draw()
    alien1.draw()

def update():

  # We controleren hier of het pijltje naar rechts wordt in gedrukt.
  if keyboard.right:
    # Als dat het geval is bewegen we de alien naar rechts door 2 bij zijn x coördinaat op te tellen.
    alien1.x += 2

  # We controleren hier of het pijltje naar links wordt in gedrukt.
  if keyboard.left:
    # Als dat het geval is bewegen we de alien naar links door 2 van zijn x coördinaat af te halen.
    alien1.x -= 2

  # We controleren hier of het pijltje naar boven wordt in gedrukt.
  if keyboard.up:
    # Als dat het geval is bewegen we de alien naar boven door 2 van zijn y coördinaat af te halen.
    alien1.y -= 2

  # We controleren hier of het pijltje naar onderen wordt in gedrukt.
  if keyboard.down:
    # Als dat het geval is bewegen we de alien naar boven door 2 bij zijn y coördinaat op te tellen.
    alien1.y += 2