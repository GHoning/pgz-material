WIDTH = 1648
HEIGHT = 450

background = Actor('background_race')

alien1 = Actor('alien1', (0, 225))
alien2 = Actor('alien2', (0, 225))
alien2.vx = 0

def draw():
    background.draw()
    alien1.draw()
    alien2.draw()

def update():
  alien1.x += 2
  alien2.vx += 0.1
  alien2.x += alien2.vx
