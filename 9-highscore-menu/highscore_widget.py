from pgzero.builtins import keyboard, keys

class HighscoreWidget():
    def __init__(self, pos, score, stop, **kwargs):
        # Variabelen en constanten die gebruikt worden door de highscore widget.
        self.ALPHABET = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
        self.stop = stop
        self.positions = [pos, (pos[0] +50, pos[1]), (pos[0] +100, pos[1])]
        self.text_pos = (pos[0]+ 50, pos[1] - 100)
        self.score = score
        self.characters = [0, 0, 0]
        self.selected = 0
        self.pressed = False

    def update(self):
        # Hiermee scrollen we door de widget heen.
        if keyboard.up and not self.pressed:
            self.pressed = True
            if self.characters[self.selected] > 24:
                self.characters[self.selected] = 0
            else:
                self.characters[self.selected] += 1
        elif keyboard.down and not self.pressed:
            self.pressed = True
            if self.characters[self.selected] < 1:
                self.characters[self.selected] = 25
            else:
                self.characters[self.selected] -= 1
        elif keyboard.left and not self.pressed:
            self.pressed = True
            if self.selected > 0:
                self.selected -= 1
        elif keyboard.right and not self.pressed:
            self.pressed = True
            if self.selected < 2:
                self.selected += 1
        # Hier accepteren we de naam van de speler en schrijven we de behaalde highscore weg.
        elif keyboard[keys.RETURN] and not self.pressed:
            self.pressed = True
            # We openen het highscore bestand.
            file = open("highscores.txt", "r")
            # We lezen de inhoud van het bestand.
            lines = file.readlines()
            # We sluiten de file weer.
            file.close()
            # Hier pluizen we de inhoud van het highscore bestand uit elkaar in de namen en scores van de spelers.
            names = []
            scores = []
            for line in lines:
                result = line.split("...")
                names.append(result[0])
                scores.append(int(result[1]))

            # We voegen de door de huidige speler behaalde score toe.
            scores.append(self.score)
            # We sorteren de scores van hoog naar laag.
            scores.sort(reverse=True)
            # We filteren de namen zodat ze weer bij de juiste score staan.
            names.insert(scores.index(self.score) , self.ALPHABET[self.characters[0]] + self.ALPHABET[self.characters[1]] + self.ALPHABET[self.characters[2]])
            # Nu hebben we elf namen met een score we willen er maar tien onthouden dus we knippen er een af.
            names = names[0:10]

            # We openen het highscore betand weer.
            file = open("highscores.txt", "w")
            # We schrijven de naam en score van de spelers weer in het highscore bestand.
            for index, name in enumerate(names):
                file.write(name + "..." + str(scores[index]) + "\n")
            # Na het schrijven sluiten we het highscore bestand weer.
            file.close()
            # Close to game from here.
            self.stop()
        elif not keyboard.up and not keyboard.down and not keyboard.left and not keyboard.right and not keyboard[keys.RETURN]:
            self.pressed = False


    def draw(self, screen):
        screen.draw.text(
            "Congratulations you got a highscore!\n use the arrow keys to select a name\n press \"enter\" to save your highscore",
            color='white',
            center=self.text_pos,
            fontsize=30,
            shadow=(1, 1)
        )

        for index, char in enumerate(self.characters):
            if index == self.selected:
                screen.draw.text(
                    str(self.ALPHABET[char]),
                    color='white',
                    center=self.positions[index],
                    fontsize=45,
                    shadow=(1, 1)
                )
            else:
                screen.draw.text(
                    str(self.ALPHABET[char]),
                    color='white',
                    center=self.positions[index],
                    fontsize=30,
                    shadow=(1, 1)
                )
