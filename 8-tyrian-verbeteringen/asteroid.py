from pgzero.builtins import Actor

class Asteroid(Actor):
    def __init__(self, image, maxHeight):
        Actor.__init__(self, image)
        self.speed = 2
        self.maxHeight = maxHeight

    def update(self, asteroids):
        self.y += self.speed
        # Als de asteroïde van het veld af is:
        if self.y > self.maxHeight:
            # Verwijderen we de asteroïde.
            asteroids.remove(self)
