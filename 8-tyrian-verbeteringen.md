## Tyrian verbeteringen
We gaan de game die we in de vorige opdracht gemaakt hebben verbeteren. We gaan beginnen met het opruimen van de code. Hiervoor gaan we alle game elementen opdelen in verschillende classes. De assets voor game heb je als het goed is nog maar mocht je ze zoeken ze staan [hier](8-tyrian-verbeteringen/tyrian_assets.zip).

### Klassen
Klassen zijn een blauwdruk van een object in onze game. Het handige van het maken van een klasse is dat we er makkelijk meer van hetzelfde soort objecten kunnen gebruiken. Deze objecten worden dan een instantie genoemd van de desbetreffende klasse. Door het gebruik van klasse kunnen we ook de code opsplitsen in kleinere stukjes. Zo kunnen we bijvoorbeeld alle code die te maken heeft met het ruimteschip in een apart bestand doen. Dit zorgt ervoor dat we niet een heel groot document hebben om in te werken. Een klasse kan ook een ouder hebben en erft dan de eigenschappen(variabelen en functies) van zijn ouder. Een klasse aanmaken in python doe je zo:
```python
class Spaceship(Actor):
    pass
```
In dit geval erft het ruimteschip van Actor. Deze klasse herken je vast nog wel.

### Spaceship
Laten we beginnen met het splitsen van onze code met het ruimteschip. Maak een nieuw bestand aan genaamd ```spaceship.py```. Daarna gaan we terug naar ```tyrian.py``` maak hiervoor de zekerheid maar een backup van door het te kopiëren of dupliceren. We gaan nu alles wat te maken heeft met het ruimteschip verplaatsen naar ```spaceship.py```. Deze komt er zo uit te zien:
```python
# We importen drie dingen uit pgzero: de Actor klasse, het keyboard en sounds.
from pgzero.builtins import Actor, keyboard, sounds

# Dit is hoe we onze klasse beginnen.
class Spaceship(Actor):
    # Dit is de init functie, deze wordt als allereerst aangeroepen zodra er een nieuwe instantie aangemaakt wordt. Voor het starten van een ruimteschip instantie hebben we drie dingen nodig: een plaatje, de hoogte en breedte van het speelveld.
    def __init__(self, image, SCREENWIDTH, SCREENHEIGHT):
        # Hiermee vertellen we dat de Actor zichzelf ook mag gaan starten, dit zodat we gebruik kunnen maken van de eigenschappen van de Actor.
        Actor.__init__(self,image)
        # Dit zijn de variabelen die we al hadden voor ons ruimteschip. Er is wel een ding veranderd. We zeggen nu self.<variabele naam> hier zeggen we dat het een eigenschap is van zichzelf, het ruimteschip dus.
        self.MAX_X_SPEED = 15
        self.MAX_Y_SPEED = 15
        self.X_ACCELERATION = 1
        self.Y_ACCELERATION = 1
        self.AIR_RESISTANCE = 0.5
        self.BLOCKED_UP = False
        self.BLOCKED_DOWN = False
        self.BLOCKED_RIGHT = False
        self.BLOCKED_LEFT = False
        self.SCREENWIDTH = SCREENWIDTH
        self.SCREENHEIGHT = SCREENHEIGHT
        self.x = 500
        self.y = 800
        self.x_speed = 0
        self.y_speed = 0

        self.PROJECTILE_SPEED = 16
        self.fired = False

    # We maken een aparte update loop voor ons ruimteschip.
    def update(self, projectiles):
        # Als we spatiebalk indrukken schieten we een nieuw projectiel.
        if keyboard.space:
            if not self.fired:
                self.fired = True
                new_projectile = Actor('projectile')
                new_projectile.x = self.x
                new_projectile.y = self.y - 30
                projectiles.append(new_projectile)
                sounds.laser.play()
        else:
            self.fired = False

        # Als we het pijltje naar rechts indrukken en we worden niet tegen gehouden:
        if keyboard.right and not self.BLOCKED_RIGHT:
            # Dan kunnen we weer naar links bewegen.
            self.BLOCKED_LEFT = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.x_speed < self.MAX_X_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.x_speed += self.X_ACCELERATION
        # Als we het pijltje naar links indrukken en we worden niet tegen gehouden:
        elif keyboard.left and not self.BLOCKED_LEFT:
            # Dan kunnen we weer naar rechts bewegen.
            self.BLOCKED_RIGHT = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.x_speed > -self.MAX_X_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.x_speed -= self.X_ACCELERATION
        # Als we niet rechts of links indrukken
        else:
            # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
            if self.x_speed < -1:
                self.x_speed += self.AIR_RESISTANCE
            elif self.x_speed > 1:
                self.x_speed -= self.AIR_RESISTANCE
            else:
                self.x_speed = 0

        # Als het ruimteschip tegen de rechterkant van het scherm aankomt:
        if self.x > self.SCREENWIDTH:
            # Dan stoppen we het ruimteschip daar en blokkeren we naar rechts gaan.
            self.x = self.SCREENWIDTH
            self.x_speed = 0
            self.BLOCKED_RIGHT = True
        # Als het ruimteschip tegen de linkerkant van het scherm aankomt:
        elif self.x < 0:
            # Dan stoppen we het ruimteschip daar en blokkeren we naar links gaan.
            self.x = 0
            self.x_speed = 0
            self.BLOCKED_LEFT = True
        # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
        else:
            self.x += self.x_speed

        # Als we pijltje omhoog indrukken en we worden niet tegen gehouden:
        if keyboard.up and not self.BLOCKED_UP:
            # Dan kunnen we weer naar onderen bewegen.
            self.BLOCKED_DOWN = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.y_speed < self.MAX_Y_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.y_speed += self.Y_ACCELERATION
        # Als we pijltje naar beneden indrukken en we worden niet tegen gehouden:
        elif keyboard.down and not self.BLOCKED_DOWN:
            # Dan kunnen we weer naar boven bewegen.
            self.BLOCKED_UP = False
            # En als we nog niet de maximale snelheid bereikt hebben:
            if self.y_speed > -self.MAX_Y_SPEED:
                # Dan versnellen we ons ruimteschip.
                self.y_speed -= self.Y_ACCELERATION
        # Als we niet naar boven of naar onderen indrukken
        else:
            # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
            if self.y_speed < -1:
                self.y_speed += self.AIR_RESISTANCE
            elif self.y_speed > 1:
                self.y_speed -= self.AIR_RESISTANCE
            else:
                self.y_speed = 0

        # Als het ruimteschip tegen de onderkant van het scherm aankomt:
        if self.y > self.SCREENHEIGHT:
            # Dan stoppen we het ruimteschip daar en blokkeren we naar beneden gaan.
            self.y = self.SCREENHEIGHT
            self.y_speed = 0
            self.BLOCKED_DOWN = True
        # Als het ruimteschip tegen de bovenkant van het speelveld aankomt (we nemen niet het hele scherm):
        elif self.y < 400:
            # Dan stoppen we het ruimteschip daar en blokkeren we naar boven gaan.
            self.y = 400
            self.y_speed = 0
            self.BLOCKED_UP = True
        # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
        else:
            self.y -= self.y_speed
```
#### self
Met het sleutelwoord ```self``` wordt de huidige instantie van het object bedoeld. Hiermee kan de instantie tegenzich zelf praten.

Om dit te laten werken moeten we ook nog aanpassingen maken aan onze ```tyrian.py```. Deze komt er als volgt uit te zien:
```python
import random
from Spaceship import Spaceship

# Game variabelen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variabelen.
spaceship = Spaceship('spaceship', WIDTH, HEIGHT)

# Onze achtergronden.
background = Actor('space_background')
background.y = 0
background.speed = 2

# asteroïden
ASTEROID_SPEED = 2
ASTEROID_SPAWN_RATE = 2
asteroids = []

projectiles = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Actor('asteroid')
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, ASTEROID_SPAWN_RATE)

def update_asteroids():
    global asteroids
    # Voor elke asteroïde in de lijst van asteroïden.
    for asteroid in asteroids:
        # Bewegen we de asteroïde.
        asteroid.y += ASTEROID_SPEED
        # Als de asteroïde van het veld af is:
        if asteroid.y > HEIGHT:
            # Verwijderen we de asteroïde.
            asteroids.remove(asteroid)

def update_backgound():
    background.y += background.speed

    if background.y > 1000:
        background.y = 0

def update_projectiles():
    global projectiles
    # Voor elke projectiel in de lijst van projectielen:
    for projectile in projectiles:
        # Beweeg het projectiel vooruit.
        projectile.y -= spaceship.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if projectile.y < 0:
            # Dan halen we het projectiel uit de lijst.
            projectiles.remove(projectile)


def check_for_collisions():
    global projectiles, asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        ASTEROID_SPEED += 0.25
                        if ASTEROID_SPAWN_RATE > 0.25:
                            ASTEROID_SPAWN_RATE -= 0.25

def restart_game():
    global projectiles, asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    score = 0
    asteroids = []
    projectiles = []
    game_over = False
    ASTEROID_SPAWN_RATE = 2
    ASTEROID_SPEED = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        spaceship.update(projectiles)
        update_backgound()
        update_asteroids()
        update_projectiles()
        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()

    for projectile in projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spawnen van asteroïden
spawn_asteroids()
```
Zie hoe we het updaten van het schip nu verplaatst hebben naar ```spaceship.py```. Dit ruimt een hoop op, controleer of het spel nog werkt. Als dat zo is kun je verder met het opruimen van de code.

### Projectielen
We gaan nu de projectielen uit de code halen en naar hun eigen klasse verplaatsen, maak een nieuw bestand aan en noem het ```projectile.py``` zet het volgende in het bestand.
```python
from pgzero.builtins import Actor

class Projectile(Actor):
    def __init__(self, image):
        Actor.__init__(self, image)
        self.PROJECTILE_SPEED = 16

    def update_projectile(self, projectiles):
        # Beweeg het projectiel vooruit.
        self.y -= self.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if self.y < 0:
            # Dan halen we het projectiel uit de lijst.
            projectiles.remove(self)
```
Om dit te laten werken moeten we nog een aantal aanpassingen doen. In ons ruimteschip moeten we nog vertellen dat we in plaats van een ```Actor``` een ```Projectile``` gebruiken. Hiervoor moeten we een import toevoegen aan ```spaceship.py```.
```python
from projectile import Projectile
```
Daarna moeten we het deel waar we het projectiel aanmaken veranderen naar het volgende:
```python
# Als we spatiebalk indrukken schieten we een nieuw projectiel.
        if keyboard.space:
            if not self.fired:
                self.fired = True
                new_projectile = Projectile('projectile')
                new_projectile.x = self.x
                new_projectile.y = self.y - 30
                projectiles.append(new_projectile)
                sounds.laser.play()
        else:
            self.fired = False
```
Daarna halen we de ```update_projectiles()``` uit ```tyrian.py```. Deze komt er dan als volgt uit te zien:
```python
import random
from Spaceship import Spaceship

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Spaceship('spaceship', WIDTH, HEIGHT)

# Onze achtergronden.
background = Actor('space_background')
background.y = 0
background.speed = 2

# asteroïden
ASTEROID_SPEED = 2
ASTEROID_SPAWN_RATE = 2
asteroids = []

projectiles = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Actor('asteroid')
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, ASTEROID_SPAWN_RATE)

def update_asteroids():
    global asteroids
    # Voor elke asteroïde in de lijst van asteroïden.
    for asteroid in asteroids:
        # Bewegen we de asteroïde.
        asteroid.y += ASTEROID_SPEED
        # Als de asteroïde van het veld af is:
        if asteroid.y > HEIGHT:
            # Verwijderen we de asteroïde.
            asteroids.remove(asteroid)

def update_backgound():
    background.y += background.speed

    if background.y > 1000:
        background.y = 0

def check_for_collisions():
    global projectiles, asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        ASTEROID_SPEED += 0.25
                        if ASTEROID_SPAWN_RATE > 0.25:
                            ASTEROID_SPAWN_RATE -= 0.25

def restart_game():
    global projectiles, asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    score = 0
    asteroids = []
    projectiles = []
    game_over = False
    ASTEROID_SPAWN_RATE = 2
    ASTEROID_SPEED = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        spaceship.update(projectiles)
        for projectile in projectiles:
            projectile.update(projectiles)

        update_backgound()
        update_asteroids()
        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()

    for projectile in projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spawnen van asteroïden
spawn_asteroids()
```

### Asteroïden
Voor de asteroïden kunnen we iets soortgelijks doen. We kunnen beginnen met het aanmaken van een nieuw bestand genaamd: ```asteroid.py```. Hierin zetten we het volgende:
```python
from pgzero.builtins import Actor

class Asteroid(Actor):
    def __init__(self, image, maxHeigth):
        Actor.__init__(self, image)
        self.speed = 2
        self.maxHeigth = maxHeigth

    def update(self, asteroids):
        self.y += self.speed
        # Als de asteroïde van het veld af is:
        if self.y > self.maxHeigth:
            # Verwijderen we de asteroïde.
            asteroids.remove(self)
```
Dan moeten we ```tyrian.py``` nog aanpassen. We halen  de ```update_asteroids()``` functie er uit, en we passen de ```update``` en ```spawn_asteroids()``` aan. De code komt er dan als volgt uit te zien:
```python
import random
from spaceship import Spaceship
from asteroid import Asteroid

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Spaceship('spaceship', WIDTH, HEIGHT)

# Onze achtergronden.
background = Actor('space_background')
background.y = 0
background.speed = 2

# Asteroïden
asteroid_spawn_rate = 2
asteroid_speed = 2
asteroids = []

projectiles = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Asteroid('asteroid', HEIGHT)
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        new_asteroid.speed = asteroid_speed
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, asteroid_spawn_rate)


def update_backgound():
    background.y += background.speed

    if background.y > 1000:
        background.y = 0

def check_for_collisions():
    global projectiles, asteroids, game_over, score, asteroid_spawn_rate, asteroid_speed
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        asteroid_speed += 0.25
                        if asteroid_spawn_rate > 0.25:
                            asteroid_spawn_rate -= 0.25

def restart_game():
    global projectiles, asteroids, game_over, score, asteroid_spawn_rate, asteroid_speed
    score = 0
    asteroids = []
    projectiles = []
    game_over = False
    asteroid_spawn_rate = 2
    asteroid_speed = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        spaceship.update(projectiles)

        for projectile in projectiles:
            projectile.update(projectiles)

        for asteroid in asteroids:
            asteroid.update(asteroids)

        update_backgound()
        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    background2.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()

    for projectile in projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spawnen van asteroïden
spawn_asteroids()
```

### Achtergronden
Het laatste dat we gaan verplaatsen naar een apart bestand zijn de Achtergronden. We maken een nieuw bestand aan met de naam ```scrolling_backgrounds.py``` hierin zetten we het volgende:
```python
from pgzero.builtins import Actor

class ScrollingBackground():
    def __init__(self, image):
        self.background = Actor(image)
        self.background.y = 0
        self.speed = 2

    def update(self):
        self.background.y += self.speed

        if self.background.y > 1000:
            self.background.y = 0

    def draw(self):
        self.background.draw()
```
Om de nieuwe achtergrond te gaan gebruiken moeten we weer een aanpassing doen aan ```tyrian.py``` we halen de background Actors en alles wat er mee te maken heeft er uit een voegen de ```draw()``` en ```update()``` toe aan die van de game.
```python
import random
from spaceship import Spaceship
from asteroid import Asteroid
from scrolling_background import ScrollingBackground

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Spaceship('spaceship', WIDTH, HEIGHT)

# Onze achtergronden.
background = ScrollingBackground('space_background')

# Asteroïden
asteroid_spawn_rate = 2
asteroid_speed = 2
asteroids = []

projectiles = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Asteroid('asteroid', HEIGHT)
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        new_asteroid.speed = asteroid_speed
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, asteroid_spawn_rate)


def check_for_collisions():
    global projectiles, asteroids, game_over, score, asteroid_spawn_rate, asteroid_speed
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        asteroid_speed += 0.25
                        if asteroid_spawn_rate > 0.25:
                            asteroid_spawn_rate -= 0.25

def restart_game():
    global projectiles, asteroids, game_over, score, asteroid_spawn_rate, asteroid_speed
    score = 0
    asteroids = []
    projectiles = []
    game_over = False
    asteroid_spawn_rate = 2
    asteroid_speed = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        background.update()
        spaceship.update(projectiles)

        for projectile in projectiles:
            projectile.update(projectiles)

        for asteroid in asteroids:
            asteroid.update(asteroids)

        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()

    for asteroid in asteroids:
        asteroid.draw()

    for projectile in projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spawnen van asteroïden
spawn_asteroids()
```
 Nu we de code opgeruimd hebben kunnen we in het volgende hoofdstuk werken aan een highscore systeem en een menu voor ons spel zodat we de highscores kunnen bekijken.

[<- Terug](7-tyrian.md) - [Volgende ->](9-highscore-menu.md)

##### Extra uitdaging
Voeg een pickup toe aan het spel, er is een kans dat deze spawnt zodra je een asteroïde kapot schiet. Zodra de speler dit oppakt krijgt hij vijf punten.

<details><summary>Hint</summary>
Maak een nieuw object genaamd ```pickup``` deze erft van ```Actor```.
</details>

<details><summary>Hint</summary>
Voor het kijken naar botsingen kun je nog eens kijken naar hoe we de projectiles opgelost hebben.
</details>

#### Credits
De achtergrond is gemaakt door [Marja]( https://www.instagram.com/marjavdhoning/). Het plaatje voor het ruimteschip, het projectiel en de explosie zijn gemaakt door [CDmir](https://opengameart.org/users/cdmir) en komen uit zijn [asteroids-game-sprites-atlas](https://opengameart.org/content/asteroids-game-sprites-atlas). Het geluid voor de laser komen van [laser-fire](https://opengameart.org/content/laser-fire) en zijn gemaakt door [dklon](https://opengameart.org/users/dklon). Het geluid voor de explosie komt van [explosion-0](https://opengameart.org/content/explosion-0) en is gemaakt door [TinyWorlds](https://opengameart.org/users/tinyworlds). Het munt op pak geluid is gemaakt door [Luke.RUSTLTD](https://opengameart.org/users/lukerustltd) het plaatje van de munt is gemaakt door [Kenney](https://kenney.nl).
