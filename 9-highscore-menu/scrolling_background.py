from pgzero.builtins import Actor

class ScrollingBackground():
    def __init__(self, image):
        self.background1 = Actor(image)
        self.background2 = Actor(image)
        self.background2.y = -500
        self.speed = 2

    def update(self):
        self.background1.y += self.speed
        self.background2.y += self.speed

        if self.background1.y > 1500:
            self.background1.y = -500

        if self.background2.y > 1500:
            self.background2.y = -500
            
    def draw(self):
        self.background1.draw()
        self.background2.draw()