## Botsingen
Om een spel te maken is het leuk als de Actoren interactie hebben met elkaar. Zoals het oppakken van een munt of een sleutel.

### Controleren op botsingen
Om te zien of de speler een munt aanraakt kunnen we gebruik maken van de ```Actor1.colliderect(Actor2)```. Met deze method kunnen we controleren of Actor1, Actor2 aanraakt.

### Speler
Laten we nu beginnen met het maken van een speler waarmee we door de spelwereld kunnen bewegen. Zet de volgende code in een nieuw bestand, en download de [acthergrond](6-botsingen/images/background.png) en de [speler](6-botsingen/images/alien.png) en zet ze in de ```Images``` folder.
```python
# We doen deze waardes met hoofdletters omdat ze constant zijn en dus niet veranderen tijdens het spelen.
WIDTH = 1648
HEIGHT = 450
PLAYER_START_POS = (200, 325)
GRAVITY = 0.3

# We laden de achtergrond.
background = Actor('background')
# We laden de speler met start waardes.
alien = Actor('alien_right', PLAYER_START_POS)
alien.vx = 0
alien.vy = 0
alien.jumped = False

def draw():
  background.draw()
  alien.draw()

def update():
  # We berekenen of er een verandering plaats moet vinden in het x-coordinaat van de alien.
  # Als het pijltje naar rechts ingedrukt wordt veranderen we het plaatje van de alien en tellen we 0.3 op bij zijn versnelling.
  if keyboard.right:
    alien.image = "alien_right"
    alien.vx += 0.3
  # Als het pijltje naar links ingedrukt wordt veranderen we het plaatje van de alien en trekken we 0.3 af van zijn versnelling.
  elif keyboard.left:
    alien.image = "alien_left"
    alien.vx -= 0.3
  else:
  # Als er geen toets ingedrukt is zetten we zijn versnelling op nul.
    alien.vx = 0

  # Om te voorkomen dat we te snel gaan zetten we wat limieten op zijn versnelling.
  if alien.vx > 4:
    alien.vx = 4

  if alien.vx < -4:
    alien.vx = -4

  # We verrekenen zijn versnelling om tot een nieuw x-coordinaat te komen.
  alien.x += alien.vx

  # We berekenen of er een verandering plaats moet vinden in het y-coordinaat van de alien.
  # Als we op pijltje naar boven drukken en de alien heeft nog niet gesprongen.
  if keyboard.up and not alien.jumped:
    # Zetten we zijn versnelling op -10 en gesprongen zetten we op waar.
    alien.vy -= 10
    alien.jumped = True
  elif alien.jumped:
    # Als de alien gesprongen heeft verekenen we de zwaartekracht. Zodat hij weer naar beneden kan komen.
    alien.vy += GRAVITY

  # Aleen als de alien gesprongen heeft hoeven we de versnelling te verekenen naar een Y-coordinaat.
  if alien.jumped:
    alien.y += alien.vy

  # We zetten een limiet op hoever de alien naar beneden kan vallen zodat we niet door de wereld vallen. We zetten gesprongen ook weer op niet waar. Zodat de speler weer springen.
  if alien.y >= 325:
    alien.y = 325
    alien.jumped = False

  # Omdat we van het scherm kunnen lopen is het handig om een knop te hebben waarmee we de speler terug zetten op zijn start positie.
  if keyboard.r:
    alien.pos = PLAYER_START_POS
```

### Munt op pakken
Om de munt op te kunnen pakken moeten we hem eerst wel toevoegen aan het spel. Dit doe je door een nieuwe actor toe te voegen aan de code. Daarnaast moeten we ook de code toevoegen waarmee we gaan controleren of de speler de munt aanraakt. Om te beginnen voeg het volgende toe aan het begin van de code, waar we de alien ook aanmaken. Hiervoor kun je de volgende assets gebruiken [munt.png](6-botsingen/images/coin.png) als plaatje en [coin.wav](6-botsingen/sounds/coin.wav) als het munt-op-pak geluid. Zet het plaatje in de ```Images``` folder en het geluid in de ```Sounds``` folder.
```python
# We laden de munt in en zetten de start waardes.
coin = Actor('coin', (1400, 325))
coin.picked_up = False
```
Daarna moet we de munt ook toevoegen aan de ```draw``` functie.
```python
def draw():
  background.draw()
  # Als de munt nog niet opgepakt is willen we hem op het scherm tekenen.
  if not coin.picked_up:
    coin.draw()

  alien.draw()
```
Als laatste moeten we dan nog in de update controleren of de speler de munt raakt. Voeg dit maar onder aan de ```update``` functie toe.
```python
# Als de speler en de munt elkaar aanraken en hij is nog niet opgepakt.
if alien.colliderect(coin) and not coin.picked_up:
  # Tellen we 10 op bij de global 10 en zeten we opgepakt op waar. We spelen ook nog een geluid af.
  global score
  score += 10
  coin.picked_up = True
  sounds.coin.play()
```

### Score laten zien
Nu willen we natuurlijk ook dat de speler  punten krijgt voor het oppakken van het muntje. Hiervoor maken we een variable aan waar we dit in kunnen opslaan. Zet dit maar onder het aanmaken van de munt.
```python
score = 0
```
Het is wel handig om de score ook te kunnen zien. Voeg het volgende toe aan de ```draw``` functie.
```python
screen.draw.text(
  # We maken van het getal score een tekst door.
  str(score),
  color='white',
  midtop=(WIDTH // 2, 10),
  fontsize=70,
  shadow=(1, 1)
)
```

### Complete Code
De complete code komt er dan zo uit te zien.
```python
WIDTH = 1648
HEIGHT = 450
PLAYER_START_POS = (200, 325)
GRAVITY = 0.3

background = Actor('background_race')
alien = Actor('alien_right', PLAYER_START_POS)
alien.vx = 0
alien.vy = 0
alien.jumped = False

coin = Actor('coin', (1400, 325))
coin.picked_up = False

score = 0

def draw():
  background.draw()
  if not coin.picked_up:
    coin.draw()

  alien.draw()

  screen.draw.text(
    str(score),
    color='white',
    midtop=(WIDTH // 2, 10),
    fontsize=70,
    shadow=(1, 1)
  )

def update():
  if keyboard.right:
    alien.image = "alien_right"
    alien.vx += 0.3
  elif keyboard.left:
    alien.image = "alien_left"
    alien.vx -= 0.3
  else:
    alien.vx = 0

  if alien.vx > 4:
    alien.vx = 4

  if alien.vx < -4:
    alien.vx = -4

  alien.x += alien.vx

  if keyboard.up and not alien.jumped:
    alien.vy -= 10
    alien.jumped = True
  elif alien.jumped:
    alien.vy += GRAVITY

  if alien.jumped:
    alien.y += alien.vy

  if alien.y >= 325:
    alien.y = 325
    alien.jumped = False

  if keyboard.r:
    alien.pos = PLAYER_START_POS

  if alien.colliderect(coin) and not coin.picked_up:
    global score
    score += 10
    coin.picked_up = True
    sounds.coin.play()
```

Je weet nu hoe je moet controleren of twee actoren elkaar raken, probeer de extra uitdaging of ga door naar de volgende opdracht.

[<- Terug](5-geluiden.md) - [Volgende ->](7-tyrian.md)

##### Extra uitdaging
Probeer de spikes toe te voegen. Zodra de speler de spikes raakt is de speler game over, dit laten we weten door op het scherm de tekst ```Game Over``` weer te geven we willen dan dat speler zijn character dan niet meer kan besturen. Dan zou het mooi zijn als de speler op ```R``` drukt de game zichzelf herstart.

<details><summary>Hint 1</summary>
Voor de game over tekst kun je hetzelfde doen als je ook al voor de score gedaan hebben.
</details>

<details><summary>Hint 2</summary>
Voeg een globale variable toe waarin je bij houdt of de speler game over is of niet. (True of False) Deze kun je dan controleren in de update function. Hierbij moet je dan zorgen dat je de restart toets buiten deze check houdt zodat je daar nog wel op kunt drukken ondanks dat de speler game over is.
</details>

#### Credits
Het munt op pak geluid is gemaakt door [Luke.RUSTLTD](https://opengameart.org/users/lukerustltd) alle plaatjes zijn gemaakt door [Kenney](https://kenney.nl).
