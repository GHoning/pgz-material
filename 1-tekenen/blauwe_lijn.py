WIDTH = 800
HEIGHT = 600
# De kleur die onze lijn gaat worden.
BLUE = 0, 0, 200

# Het start coördinaat van de lijn.
START = 100, 100

# Het eind coördinaat van de lijn.
END = 700, 500

def draw():
  # Nu tekenen we de lijn
  screen.draw.line(START, END, BLUE)
