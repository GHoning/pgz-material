## Mu-editor

### Downloaden en installeren
Om aan de slag te gaan met Pygame-Zero gaan we gebruik maken van de [Mu-editor](https://codewith.mu). De Mu-editor is gratis te downloaden van hun [downloads pagina](https://codewith.mu/en/download). Zodra de download klaar is voer je het bestand uit om de Installatie te starten.

### Test programma
Als de Installatie geslaagd is kunnen we controleren of alles goed gegaan is. Hiervoor gaan we een klein programma schrijven. Start de Mu-editor en zet hem in Pygame-Zero mode door dit te selecteren tijdens het opstarten of door op de "mode" knop (links boven in) te klikken en dan Pygame-Zero te kiezen.

```python
# Hiermee zetten we de breedte en hoogte van het venster.
WIDTH = 300
HEIGHT = 300

# Hier vertellen we dat hij het venster volledig rood moet maken.
def draw():
    screen.fill((255, 0, 0))
```
Sla de code op als ```venster.py``` en voer het uit door op ```Play``` te klikken. Als alles goed is gegaan zie je nu een venster van 300 bij 300 dat volledig rood is. Klik op volgende om verder te gaan naar de volgende opdracht.

[<- Terug](README.md) - [Volgende ->](1-tekenen.md)

##### Meer informatie
[Pygame-Zero documentatie](https://pygame-zero.readthedocs.io/en/stable/)  
[Mu-editor tutorials](https://codewith.mu/en/tutorials/)  
[Mu-editor how-to Guides](https://codewith.mu/en/howto/)
