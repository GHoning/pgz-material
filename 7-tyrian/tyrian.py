import random

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Actor('spaceship')
spaceship.MAX_X_SPEED = 15
spaceship.MAX_Y_SPEED = 15
spaceship.X_ACCELERATION = 1
spaceship.Y_ACCELERATION = 1
spaceship.AIR_RESISTANCE = 0.5
spaceship.BLOCKED_UP = False
spaceship.BLOCKED_DOWN = False
spaceship.BLOCKED_RIGHT = False
spaceship.BLOCKED_LEFT = False
spaceship.x = WIDTH/2
spaceship.y = 800
spaceship.x_speed = 0
spaceship.y_speed = 0
spaceship.PROJECTILE_SPEED = 16
spaceship.projectiles = []
spaceship.fired = False

# Onze achtergrond
background = Actor('space_background')
background.y = 0
background.speed = 2

# asteroïden
ASTEROID_SPEED = 2
ASTEROID_SPAWN_RATE = 2
asteroids = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Actor('asteroid')
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, ASTEROID_SPAWN_RATE)
        
def update_asteroids():
    global asteroids
    # Voor elke asteroïde in de lijst van asteroïden.
    for asteroid in asteroids:
        # Bewegen we de asteroïde.
        asteroid.y += ASTEROID_SPEED
        # Als de asteroïde van het veld af is:
        if asteroid.y > HEIGHT:
            # Verwijderen we de asteroïde.
            asteroids.remove(asteroid)

# We maken een aparte update loop voor ons ruimteschip.
def update_spaceship():
    # Als we spatiebalk indrukken schieten we een nieuw projectiel.
    if keyboard.space:
        if not spaceship.fired:
            spaceship.fired = True
            new_projectile = Actor('projectile')
            new_projectile.x = spaceship.x
            new_projectile.y = spaceship.y - 30
            spaceship.projectiles.append(new_projectile)
            sounds.laser.play()
    else:
        spaceship.fired = False
    
    # Als we het pijltje naar rechts indrukken en we worden niet tegengehouden:
    if keyboard.right and not spaceship.BLOCKED_RIGHT:
        # Dan kunnen we weer naar links bewegen.
        spaceship.BLOCKED_LEFT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed < spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed += spaceship.X_ACCELERATION
    # Als we het pijltje naar links indrukken en we worden niet tegengehouden:
    elif keyboard.left and not spaceship.BLOCKED_LEFT:
        # Dan kunnen we weer naar rechts bewegen.
        spaceship.BLOCKED_RIGHT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed > -spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed -= spaceship.X_ACCELERATION
    # Als we niet rechts of links indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.x_speed < -1:
            spaceship.x_speed += spaceship.AIR_RESISTANCE
        elif spaceship.x_speed > 1:
            spaceship.x_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.x_speed = 0

    # Als het ruimteschip tegen de rechterkant van het scherm aankomt:
    if spaceship.x > WIDTH:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar rechts gaan.
        spaceship.x = WIDTH
        spaceship.x_speed = 0
        spaceship.BLOCKED_RIGHT = True
    # Als het ruimteschip tegen de linkerkant van het scherm aankomt:
    elif spaceship.x < 0:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar links gaan.
        spaceship.x = 0
        spaceship.x_speed = 0
        spaceship.BLOCKED_LEFT = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.x += spaceship.x_speed

    # Als we pijltje omhoog indrukken en we worden niet tegengehouden:
    if keyboard.up and not spaceship.BLOCKED_UP:
        # Dan kunnen we weer naar onderen bewegen.
        spaceship.BLOCKED_DOWN = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed < spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
           spaceship.y_speed += spaceship.Y_ACCELERATION
    # Als we pijltje naar beneden indrukken en we worden niet tegengehouden:
    elif keyboard.down and not spaceship.BLOCKED_DOWN:
        # Dan kunnen we weer naar boven bewegen.
        spaceship.BLOCKED_UP = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed > -spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.y_speed -= spaceship.Y_ACCELERATION
    # Als we niet naar boven of naar onderen indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.y_speed < -1:
            spaceship.y_speed += spaceship.AIR_RESISTANCE
        elif spaceship.y_speed > 1:
            spaceship.y_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.y_speed = 0

    # Als het ruimteschip tegen de onderkant van het scherm aankomt:
    if spaceship.y > HEIGHT:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar beneden gaan.
        spaceship.y = HEIGHT
        spaceship.y_speed = 0
        spaceship.BLOCKED_DOWN = True
    # Als het ruimteschip tegen de bovenkant van het speelveld aankomt (we nemen niet het hele scherm):
    elif spaceship.y < 400:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar boven gaan.
        spaceship.y = 400
        spaceship.y_speed = 0
        spaceship.BLOCKED_UP = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.y -= spaceship.y_speed
    
    
def update_backgound():
    background.y += background.speed
    
    if background.y > 1000:
        background.y = 0
        
        
def update_projectiles():
    # Voor elke projectiel in de lijst van projectielen:
    for projectile in spaceship.projectiles:
        # Beweeg het projectiel voor uit.
        projectile.y -= spaceship.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if projectile.y < 0:
            # Dan halen we het projectiel uit de lijst.
            spaceship.projectiles.remove(projectile)
        
        
def check_for_collisions():
    global asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"
            
            # Voor projectiel in de lijst van projectielen.
            for projectile in spaceship.projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        spaceship.projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        ASTEROID_SPEED += 0.25
                        if ASTEROID_SPAWN_RATE > 0.25:
                            ASTEROID_SPAWN_RATE -= 0.25
        
def restart_game():
    global asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    score = 0
    asteroids = []
    spaceship.projectiles = []
    game_over = False
    ASTEROID_SPAWN_RATE = 2
    ASTEROID_SPEED = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        update_spaceship()
        update_backgound()
        update_asteroids()
        update_projectiles()
        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()
        
    for projectile in spaceship.projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )
    
    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spwawnen van asteroïden
spawn_asteroids()
