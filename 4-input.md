## Input
Laten we eens kijken of we onze aliens ook kunnen besturen met ons toetsenbord of muis.

### Toetsenbord
We beginnen met het toetsenbord, kun je, je de bewegende alien nog herinneren? We doen bijna hetzelfde alleen nu controleren we of er een knop ingedrukt wordt om te bepalen wat we optellen of aftrekken van x en y. Dit doen we met behulp van ```keyboard```, dit object krijgen we van Pygame-Zero. Vergeet niet om de [alien](4-input/alien1.png) en de [achtergrond](4-input/background_move.png) te downloaden. Open een nieuw bestand, noem het ```keyboard.py``` en zet daar het volgende in.

```python
WIDTH = 1024
HEIGHT = 1024

background = Actor('background_move')
alien1 = Actor('alien1', (512, 512))

def draw():
    background.draw()
    alien1.draw()

def update():

  # We controleren hier of het pijltje naar rechts wordt ingedrukt.
  if keyboard.right:
    # Als dat het geval is bewegen we de alien naar rechts door 2 bij zijn x coördinaat op te tellen.
    alien1.x += 2

  # We controleren hier of het pijltje naar links wordt ingedrukt.
  if keyboard.left:
    # Als dat het geval is bewegen we de alien naar links door 2 van zijn x coördinaat af te halen.
    alien1.x -= 2

  # We controleren hier of het pijltje naar boven wordt ingedrukt.
  if keyboard.up:
    # Als dat het geval is bewegen we de alien naar boven door 2 van zijn y coördinaat af te halen.
    alien1.y -= 2

  # We controleren hier of het pijltje naar onderen wordt ingedrukt.
  if keyboard.down:
    # Als dat het geval is bewegen we de alien naar boven door 2 bij zijn y coördinaat op te tellen.
    alien1.y += 2
```

### Muis
Nu gaan we de muis gebruiken. We gaan een alien maken die we kunnen verplaatsen met de muis. We willen dat als we op de alien klikken de alien de muis cursor volgt en zodra we de linkermuis knop los laten stopt. Dit gaan we doen met behulp van drie functies die van Pygame-Zero krijgen: ```on_mouse_down```, ```on_mouse_up``` en ```on_mouse_move```. Je kunt de plaatjes hier vinden: [achtergrond](4-input/background_move.png) en [alien2](4-input/alien2.png). Open een nieuw bestand, noem het ```mouse.py``` en zet daar het volgende in.

```python
WIDTH = 1024
HEIGHT = 1024

background = Actor('background_move')
alien2 = Actor('alien2', (512, 512))
# We maken een extra variable aan op de alien om bij te houden of er gesleept wordt met de alien.
alien2.dragging = False

def draw():
    background.draw()
    alien2.draw()

# Hier gebruiken we de eerste muis gerelateerde functie van Pygame-Zero
def on_mouse_down(pos):
  # We controleren of de muis klik plaats vind op onze alien.
  if alien2.collidepoint(pos):
    # Als dat zo is beginnen we met het slepen van de alien.
    alien2.dragging = True

# Zodra we doorkrijgen dat de muisknop niet meer ingedrukt is
def on_mouse_up():
  # Stoppen we met het slepen van de alien.
  alien2.dragging = False

# Hier krijgen we door of de muis cursor zich verplaatst.
def on_mouse_move(pos):
  # We controleren of we de alien aan het slepen zijn.
  if alien2.dragging:
    # Als dat het geval is zetten we de positie van de alien gelijk aan die van de muis cursor.
    alien2.pos = pos
```

Je weet nu hoe je input kunt afvangen en gebruiken, probeer de extra uitdaging of ga door naar de volgende opdracht.

[<- Terug](3-bewegen.md) - [Volgende ->](5-geluiden.md)

##### Extra uitdaging
Maak een spel waarin een iemand de alien met de pijltjes kan besturen en je met de muis op de alien kan klikken. Je kunt het plaatje van de alien veranderen door de ```image``` property te veranderen. Dit doe je als volgt:

```python
alien1.image = "<naam van een plaatje in je images folder>"
```

<details><summary>Hint</summary>
Wat je kunt doen is het eerste voorbeeld nemen waarin we de alien met het toetsenbord bewegen. Daar voeg je dan de functie toe waarmee we controleren of er met de linker muisknop ingedrukt wordt aan toe. Hierin controleren we dan of de muisklik plaatsvindt op de alien die we bewegen.
</details>

#### Credits
Alle plaatjes zijn gemaakt door [Kenney](https://kenney.nl).
