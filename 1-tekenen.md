## Tekenen
Laten we beginnen met het tekenen van een aantal vormen, een vierkant lijkt me een prima begin.

### Rood vierkant
Open nieuw bestand door op ```New``` te klikken, deze kun je dan opslaan als ```rood_vierkant.py``` om hem uit te voeren.
```python
#Hiermee bepalen we hoe groot het venster moet worden.
WIDTH = 800
HEIGHT = 600

# Hier bepalen we de kleur die het vierkant gaat krijgen.
RED = 200, 0, 0

# Dit is de doos die we gaan tekenen zo meteen. Het eerste deel is de positie van de doos.
# Het tweede deel is de breedte en de hoogte van de doos.
BOX = Rect((20, 20), (100, 100))

# Dit is een functie, dit is een verzameling van instructies voor de computer.
# In dit geval hebben we een functie die "draw" heet, Pygame-Zero verwacht deze functie. (Daarom heeft hij een Engelse naam.)
# Hier bepalen wij wat we gaan tekenen.
def draw():
  # Wij tekenen ons vierkant in het rood.
  screen.draw.filled_rect(BOX, RED)
```

### RGB
De kleur wordt bepaald door drie getallen. Deze staan voor Rood, Groen en Blauw (of in het Engels Red, Green and Blue). Het getal staat voor de hoeveelheid we willen hebben van een kleur. Dus hoe groter het getal hoe meer van de kleur we gebruiken. Kijk [hier](https://www.w3schools.com/colors/colors_picker.asp) om te zien hoe je jouw favoriete kleur kunt maken.

### Groene cirkel
Laten we nu een groene cirkel tekenen, open weer een nieuw bestand en sla het op als ```groene_cirkel.py```, zet het onderstaande daar in:
```python
WIDTH = 800
HEIGHT = 600

# Deze kleur maken we onze cirkel.
GREEN = 0, 200, 0

# Dit is de positie van het midden van de cirkel.
POS = 100, 100

# De radius van een cirkel is de afstand vanaf het midden tot de rand van de cirkel
RADIUS = 50

def draw():
  # Dit is hoe we een cirkel tekenen.
  screen.draw.filled_circle(POS, RADIUS, GREEN)
```

###  Coördinaten
Een coördinaat bestaat uit twee getallen. Deze noemen we de x en y  omdat ze op die as liggen. Het coördinaat 0, 0 ligt in de linker boven hoek. Het standaard venster is 800 bij 600 pixels dus het coördinaat in de rechter onder hoek is 800, 600.

![x,y en assen](1-tekenen/axis.png)

### Blauwe lijn
Tijd om een lijn te tekenen, open weer een nieuw bestand en sla het op als ```blauwe_lijn.py```, zet het onderstaande daar in:
```python
WIDTH = 800
HEIGHT = 600
# De kleur die onze lijn gaat worden.
BLUE = 0, 0, 200

# Het start coördinaat van de lijn.
START = 100, 100

# Het eind coördinaat van de lijn.
END = 700, 500

def draw():
  # Nu tekenen we de lijn
  screen.draw.line(START, END, BLUE)
```
Je weet nu hoe je vormen kunt tekenen met Pygame-Zero. Probeer de extra uitdaging of ga verder naar de volgende opdracht.

[<- Terug](0-installatie.md) - [Volgende ->](2-actoren.md)

##### Extra uitdaging
Schrijf een programma waarmee je alle bovenstaande vormen tegelijk tekenent. Het lastige is dat je maar een ```draw()``` kunt hebben, anders raakt de computer in de war.


<details><summary>Hint</summary>
Probeer de draw functies van de drie programma's samen te voegen, het ziet er dan ongeveer zo uit:

```python
def draw():
  screen.draw.filled_rect(BOX, RED)
  screen.draw.filled_circle(POS, RADIUS, GREEN)
  screen.draw.line(START, END, BLUE)
```
</details>
