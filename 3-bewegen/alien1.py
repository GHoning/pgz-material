WIDTH = 1648
HEIGHT = 450

background = Actor('background_race')
# We beginnen deze keer bij 0, 480 zodat we genoeg ruimte hebben om op te bewegen.
alien1 = Actor('alien1', (0, 480))

def draw():
    background.draw()
    alien1.draw()

# De update functie kun je gebruiken om van alles te updaten op je speelveld zoals de positie van onze alien.
def update():
  # We tellen 2 op bij het x-coördinaat, hierdoor gaat de alien naar rechts.
  alien1.x = alien1.x + 2
  # We halen 1 af van het y-coördinaat, hierdoor gaat de alien naar boven.
  alien1.y = alien1.y - 1
