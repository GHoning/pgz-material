from pgzero.builtins import keyboard, sounds, clock, keys
from spaceship import Spaceship
from projectile import Projectile
from scrolling_background import ScrollingBackground
from asteroid import Asteroid
from highscore_widget import HighscoreWidget
import random

class TyrianGame():

    def __init__(self, width, height, stop):
        self.stop = stop
        self.game_over = False
        self.score = 0
        self.width = width
        self.height = height
        # Ons ruimteschip met al zijn variablen.
        self.spaceship = Spaceship('spaceship', width, height)

        # Onze achtergronden.
        self.background = ScrollingBackground('space_background')

        # Asteroïden
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.asteroids = []

        # Projectielen
        self.projectiles = []

        self.widget = None
        self.new_high = False

    def spawn_asteroids(self):
        # Als de game nog niet voor bij is:
        if not self.game_over:
            # Dan spawnen we een nieuwe asteroïde in.
            new_asteroid = Asteroid('asteroid', self.height)
            new_asteroid.x = random.randint(0, self.width)
            new_asteroid.y = 0
            new_asteroid.speed = self.asteroid_speed
            # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
            self.asteroids.append(new_asteroid)
            # We zetten een timer voor het spawnen van een nieuwe asteroïde.
            clock.schedule_unique(self.spawn_asteroids, self.asteroid_spawn_rate)

    def start_game(self):
        self.score = 0
        self.asteroids = []
        self.spaceship.projectiles = []
        self.game_over = False
        self.asteroid_spawn_rate = 2
        self.asteroid_speed = 2
        self.spaceship.image = "spaceship"
        self.spawn_asteroids()
        self.spaceship.x = self.width/2
        self.spaceship.y = self.height/2 + 400

    def new_highscore(self, score):
        highscore_file = open("highscores.txt", "r")
        last_line = highscore_file.readlines()[9]
        highscore_file.close()
        old_score = int(last_line.split("...")[1])

        if score > old_score:
            return True
        else:
            return False


    def check_for_collisions(self):
        # Voor asteroïde in asteroïden
        for asteroid in self.asteroids:
            if asteroid is not None:
                # Als asteroïde het ruimteschip raakt:
                if asteroid.colliderect(self.spaceship):
                    # Is het spel over en explodeert het ruimteschip.
                    self.game_over = True
                    sounds.explosion.play()
                    self.spaceship.image = "explosion"
                    if self.new_highscore(self.score):
                        # Spawn the highscore widget. If the score succeeds the smallest stored score.
                        self.new_high = True
                        self.widget = HighscoreWidget((self.width/2,600), self.score, self.stop)


                # Voor projectiel in de lijst van projectielen.
                for projectile in self.projectiles:
                    if projectile is not None:
                        # Als asteroïde geraakt wordt door het projectiel.
                        if asteroid.colliderect(projectile):
                            # Spelen we een geluid af
                            sounds.explosion.play()
                            # Halen we de asteroïde en het projectiel uit de lijst.
                            self.asteroids.remove(asteroid)
                            self.projectiles.remove(projectile)
                            # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                            self.score += 1
                            self.asteroid_speed += 0.25
                            if self.asteroid_spawn_rate > 0.25:
                                self.asteroid_spawn_rate -= 0.25


    def update(self):
        # Als de game nog niet voor bij is updaten we ons ruimteschip en andere spel elementen.
        if not self.game_over:
            self.spaceship.update(self.projectiles)
            self.background.update()

            # Voor elke projectiel in de lijst van projectielen:
            for projectile in self.projectiles:
                projectile.update(self.projectiles)

            # Voor elke asteroide in de lijst van asteroiden:
            for asteroid in self.asteroids:
                asteroid.update(self.asteroids)

            self.check_for_collisions()
        elif self.widget is not None:
            self.widget.update()

        if keyboard[keys.RETURN] and self.game_over and not self.new_high:
            self.stop()


    def draw(self, screen):
        # We tekenen de achtergrond en het ruimteschip.
        screen.clear()
        self.background.draw()
        self.spaceship.draw()
        for asteroid in self.asteroids:
            asteroid.draw()

        for projectile in self.projectiles:
            projectile.draw()

        screen.draw.text(
            str(self.score),
            color='white',
            midtop =(self.width/2, 10),
            fontsize=70,
        )

        if self.game_over and self.widget is not None and self.new_high:
            self.widget.draw(screen)
        elif self.game_over and not self.new_high:
            screen.draw.text(
                "Game Over\nPress \"Enter\" to quit",
                color='white',
                midtop =(self.width/2, self.height/2),
                fontsize=70,
            )
