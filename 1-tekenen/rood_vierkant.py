#Hiermee bepalen we hoe groot het venster moet worden.
WIDTH = 800
HEIGHT = 600

# Hier bepalen we de kleur die het vierkant gaat krijgen.
RED = 200, 0, 0

# Dit is de doos die we gaan tekenen zo meteen. Het eerste deel is de positie van de doos.
# Het tweede deel is de breedte en de hoogte van de doos.
BOX = Rect((20, 20), (100, 100))

# Dit is een functie, dit is een verzameling van instructies voor de computer.
# In dit geval hebben we een functie die "draw" heet, Pygame-Zero verwacht deze functie. (Daarom heeft hij een Engelse naam.)
# Hier bepalen wij wat we gaan tekenen.
def draw():
  # Wij tekenen ons vierkant in het rood.
  screen.draw.filled_rect(BOX, RED)
