WIDTH = 1648
HEIGHT = 450

background = Actor('background_race')
alien2 = Actor('alien2', (0, 225))
alien2.vx = 0
alien2.vy = 0

def draw():
  background.draw()
  alien2.draw()

def update():
  # Dit is een afkorting voor alien2.vx = alien2.vx  + 0.5
  alien2.vx += 0.5
  alien2.vy += 0.05

  alien2.x += alien2.vx
  alien2.y += alien2.vy
