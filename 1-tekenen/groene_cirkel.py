WIDTH = 800
HEIGHT = 600

# Deze kleur maken we onze cirkel.
GREEN = 0, 200, 0

# Dit is de positie van het midden van de cirkel.
POS = 100, 100

# De radius van een cirkel is de afstand van af het midden tot de rand van de cirkel
RADIUS = 50

def draw():
  # Dit is hoe we een cirkel tekenen.
  screen.draw.filled_circle(POS, RADIUS, GREEN)
