import sys

from tyrian_game import TyrianGame
from button import Button

# Game variablen.
TITLE = 'tyrian++'
WIDTH = 900
HEIGHT = 1000


def start_game():
    global runningGame
    runningGame = True
    game.start_game()

def stop_game():
    global runningGame
    runningGame = False

def show_highscore():
    global showingHighscore
    showingHighscore = True
    
def hide_highscore():
    global showingHighscore
    showingHighscore = False
    
def quit_game():
    sys.exit()


startButton = Button("Start",(WIDTH/2 - 100, 100),(200, 100), "grey",start_game)
highscoreButton = Button("Highscore",(WIDTH/2 - 100, 300),(200, 100), "grey", show_highscore)
stopButton = Button("Quit",(WIDTH/2 - 100, 500),(200, 100), "grey", quit_game)
backButton = Button("Back",(WIDTH/2 - 100, 600),(200, 100), "grey", hide_highscore)

game = TyrianGame(WIDTH, HEIGHT, stop_game)
runningGame = False
showingHighscore = False

def update():
    if runningGame:
        game.update()
    elif showingHighscore:
        backButton.update()
    else:
        startButton.update()
        highscoreButton.update()
        stopButton.update()


def draw():
    screen.clear()
    if runningGame:
        game.draw(screen)
    elif showingHighscore:
        highscore_file = open("highscores.txt", "r")
        lines = highscore_file.readlines()
        highscore_file.close()
        for index, line in enumerate(lines):
            screen.draw.text(
                str(line),
                color='white',
                midtop =(WIDTH/2, 100 + 40 * index),
                fontsize=40,
            )
        backButton.draw(screen)
    else:
        startButton.draw(screen)
        highscoreButton.draw(screen)
        stopButton.draw(screen)