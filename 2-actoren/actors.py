WIDTH = 640
HEIGHT = 480

# We maken een achtergrond aan.
background = Actor('background')

# We maken een speler aan.
player = Actor('player', (340, 240))

def draw():
    # We vertellend de computer dat ze getekend moeten worden.
    background.draw()
    player.draw()
