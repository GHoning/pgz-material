## Tyrian
We gaan tyrian bouwen, tyrian is een game waarin je met een ruimteschip asteroïden kapot moet schieten voor ze de aarde raken. Laten we beginnen met het ruimteschip waarin we rond kunnen vliegen over het speelveld.

### Het ruimteschip
Open een nieuw bestand en noem het ```tyrian.py``` en zet de onderstaande code er in. Voor dit project hebben we weer plaatjes en geluiden nodig. Deze keer hebben we ze opgeslagen als een .zip bestand. Dus je moet ze nog uitpakken voor je ze kunt gebruiken. Download de zip file [hier](7-tyrian/tyrian_assets.zip). We gaan een schip maken dat in het onderste gedeelte van het venster kan rondvliegen. De code die je hiervoor nodig gaat hebben staat hieronder vermeld.
```python
import random

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False

# Ons ruimteschip met al zijn variablen.
spaceship = Actor('spaceship')
spaceship.MAX_X_SPEED = 15
spaceship.MAX_Y_SPEED = 15
spaceship.X_ACCELERATION = 1
spaceship.Y_ACCELERATION = 1
spaceship.AIR_RESISTANCE = 0.5
spaceship.BLOCKED_UP = False
spaceship.BLOCKED_DOWN = False
spaceship.BLOCKED_RIGHT = False
spaceship.BLOCKED_LEFT = False
spaceship.x = WIDTH/2
spaceship.y = 800
spaceship.x_speed = 0
spaceship.y_speed = 0

# We maken een aparte update loop voor ons ruimteschip.
def update_spaceship():
    # Als we het pijltje naar rechts indrukken en we worden niet tegengehouden:
    if keyboard.right and not spaceship.BLOCKED_RIGHT:
        # Dan kunnen we weer naar links bewegen.
        spaceship.BLOCKED_LEFT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed < spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed += spaceship.X_ACCELERATION
    # Als we het pijltje naar links indrukken en we worden niet tegengehouden:
    elif keyboard.left and not spaceship.BLOCKED_LEFT:
        # Dan kunnen we weer naar rechts bewegen.
        spaceship.BLOCKED_RIGHT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed > -spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed -= spaceship.X_ACCELERATION
    # Als we niet rechts of links indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.x_speed < -1:
            spaceship.x_speed += spaceship.AIR_RESISTANCE
        elif spaceship.x_speed > 1:
            spaceship.x_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.x_speed = 0

    # Als het ruimteschip tegen de rechterkant van het scherm aankomt:
    if spaceship.x > WIDTH:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar rechts gaan.
        spaceship.x = WIDTH
        spaceship.x_speed = 0
        spaceship.BLOCKED_RIGHT = True
    # Als het ruimteschip tegen de linkerkant van het scherm aankomt:
    elif spaceship.x < 0:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar links gaan.
        spaceship.x = 0
        spaceship.x_speed = 0
        spaceship.BLOCKED_LEFT = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.x += spaceship.x_speed

    # Als we pijltje omhoog indrukken en we worden niet tegengehouden:
    if keyboard.up and not spaceship.BLOCKED_UP:
        # Dan kunnen we weer naar onderen bewegen.
        spaceship.BLOCKED_DOWN = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed < spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
           spaceship.y_speed += spaceship.Y_ACCELERATION
    # Als we pijltje naar beneden indrukken en we worden niet tegengehouden:
    elif keyboard.down and not spaceship.BLOCKED_DOWN:
        # Dan kunnen we weer naar boven bewegen.
        spaceship.BLOCKED_UP = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed > -spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.y_speed -= spaceship.Y_ACCELERATION
    # Als we niet naar boven of naar onderen indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.y_speed < -1:
            spaceship.y_speed += spaceship.AIR_RESISTANCE
        elif spaceship.y_speed > 1:
            spaceship.y_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.y_speed = 0

    # Als het ruimteschip tegen de onderkant van het scherm aankomt:
    if spaceship.y > HEIGHT:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar beneden gaan.
        spaceship.y = HEIGHT
        spaceship.y_speed = 0
        spaceship.BLOCKED_DOWN = True
    # Als het ruimteschip tegen de bovenkant van het speelveld aankomt (we nemen niet het hele scherm):
    elif spaceship.y < 400:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar boven gaan.
        spaceship.y = 400
        spaceship.y_speed = 0
        spaceship.BLOCKED_UP = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.y -= spaceship.y_speed


def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip.
    if not game_over:
        update_spaceship()


def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    screen.blit('space_background', (0, 0))
    spaceship.draw()
```
Nu kunnen we rondvliegen in ons ruimteschip!

### Bewegende achtergrond
Om de speler het gevoel te geven dat hij rondvliegt in zijn ruimteschip gaan we de achtergrond laten bewegen. We gaan hiervoor de achtergrond gebruiken die we iedere keer weer boven aan neer zetten zodra ze onder het scherm verlaten hebben. Voeg onder de variablen voor ons ruimteschip het volgende toe:
```python
# Onze achtergrond
background = Actor('space_background')
background.y = 0
background.speed = 2
```
Voeg het volgende toe aan de code, onder de ```update_spaceship()``` functie voeg je de volgende functie toe:
```python
def update_backgound():
    background.y += background.speed

    if background.y > 1000:
        background.y = 0
```
Aan onze update loop voegen we dan de ```update_background()``` toe.
```python
def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip.
    if not game_over:
        update_spaceship()
        update_backgound()
```
Als laatste moeten we de achtergronden nog tekenen.
```python
def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
```
Als alles goed gegaan is heb je nu een achtergrond die voor eeuwig onder de speler door scrollt.

#### Import
Omdat we random asteroïden willen in spawnen gaan we gebruik maken van python zijn ```random``` library. Een library is een verzameling van functionaliteiten. In het geval van ```random``` gaan deze functies over het genereren van getallen. Voor we deze library kunnen gebruiken moeten we dit wel beschrijven in onze code. Dit doen we door gebruik te maken van ```import```. Met ```import random``` maken we de computer duidelijk dat we de ```random``` library gaan gebruiken in onze code en dat hij deze dus moet gaan importeren. Dit zetten we boven aan in ons bestand.

### De asteroïden
Nu we weten hoe we een library importen, kunnen we een uitdaging toevoegen aan ons spel. We gaan asteroïden toevoegen die ervoor zorgen dat ons schip ontploft zodra ze het schip aanraken. Voeg het volgende toe onder het aanmaken van het ruimteschip.
```python
# asteroïden
ASTEROID_SPEED = 2
ASTEROID_SPAWN_RATE = 2
asteroids = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Actor('asteroid')
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, ASTEROID_SPAWN_RATE)

def update_asteroids():
    global asteroids
    # Voor elke asteroïde in de lijst van asteroïden.
    for asteroid in asteroids:
        # Bewegen we de asteroïde.
        asteroid.y += ASTEROID_SPEED
        # Als de asteroïde van het veld af is:
        if asteroid.y > HEIGHT:
            # Verwijderen we de asteroïde.
            asteroids.remove(asteroid)
```
Nu willen we nog kunnen controleren of een van de asteroïden het ruimteschip raakt. Hiervoor gaan we nog een functie toevoegen. Voeg onder de ```update_background()``` functie de volgende functie toe:
```python
def check_for_collisions():
    global asteroids, game_over
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"
```
Dan willen we de ```update()``` functie als volgt aanpassen:
```python
def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        update_spaceship()
        update_backgound()
        update_asteroids()
        check_for_collisions()
```
Nu moeten we onze ```draw()``` functie nog aanpassen:
```python
def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()
```
We zijn er bijna nog een regel helemaal onder aan de code toevoegen.
```python
# Start het spwawnen van asteroïden
spawn_asteroids()
```
Als het goed is gegaan heb je nu asteroïden die van boven naar beneden komen. Die zodra ze je ruimteschip raken er voor zorgen dat je ontploft en de game over is.

### Schieten
Nu we gevaar lopen door de asteroïden is het tijd dat we ons ruimteschip een manier geven om zichzelf te verdedigen. Hiervoor zijn lasers een oplossing. We beginnen met het toevoegen van de volgende variablen aan het ruimteschip:
```python
spaceship.PROJECTILE_SPEED = 16
spaceship.projectiles = []
spaceship.fired = False

score = 0
```
Hierna voegen we het volgende toe aan ```update_spaceship()```:
```python
def update_spaceship():
    # Als we spatiebalk indrukken schieten we een nieuw projectiel.
    if keyboard.space:
        if not spaceship.fired:
            spaceship.fired = True
            new_projectile = Actor('projectile')
            new_projectile.x = spaceship.x - 60
            new_projectile.y = spaceship.y - 60
            spaceship.projectiles.append(new_projectile)
            sounds.laser.play()
    else:
        spaceship.fired = False
# De rest van de functie die we al hadden.
```
Nu we kunnen schieten willen we de projectielen nog updaten zodat ze vooruit gaan hiervoor voegen we een nieuwe functie toe:
```python
def update_projectiles():
    # Voor elke projectiel in de lijst van projectielen:
    for projectile in spaceship.projectiles:
        # Beweeg het projectiel vooruit.
        projectile.y -= spaceship.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if projectile.y < 0:
            # Dan halen we het projectiel uit de lijst.
            spaceship.projectiles.remove(projectile)
```
Om er voor te zorgen dat we de asteroïden kunnen raken. Dit doen we door de ```check_for_collisions()``` aan te passen:
```python
def check_for_collisions():
    global asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in spaceship.projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        spaceship.projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        ASTEROID_SPEED += 0.25
                        if ASTEROID_SPAWN_RATE > 0.25:
                            ASTEROID_SPAWN_RATE -= 0.25
```
Nu moet we de ```update()``` en ```draw()``` nog aanpassen:
```python
def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        update_spaceship()
        update_backgound()
        update_asteroids()
        update_projectiles()
        check_for_collisions()
```
```python
def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()

    for projectile in spaceship.projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )
```

### Complete Code
De complete code ziet er dan zo uit:
```python
import random

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Actor('spaceship')
spaceship.MAX_X_SPEED = 15
spaceship.MAX_Y_SPEED = 15
spaceship.X_ACCELERATION = 1
spaceship.Y_ACCELERATION = 1
spaceship.AIR_RESISTANCE = 0.5
spaceship.BLOCKED_UP = False
spaceship.BLOCKED_DOWN = False
spaceship.BLOCKED_RIGHT = False
spaceship.BLOCKED_LEFT = False
spaceship.x = WIDTH/2
spaceship.y = 800
spaceship.x_speed = 0
spaceship.y_speed = 0
spaceship.PROJECTILE_SPEED = 16
spaceship.projectiles = []
spaceship.fired = False

# Onze achtergronden.
background = Actor('space_background')
background.y = 0
background.speed = 2

# asteroïden
ASTEROID_SPEED = 2
ASTEROID_SPAWN_RATE = 2
asteroids = []

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Actor('asteroid')
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, ASTEROID_SPAWN_RATE)

def update_asteroids():
    global asteroids
    # Voor elke asteroïde in de lijst van asteroïden.
    for asteroid in asteroids:
        # Bewegen we de asteroïde.
        asteroid.y += ASTEROID_SPEED
        # Als de asteroïde van het veld af is:
        if asteroid.y > HEIGHT:
            # Verwijderen we de asteroïde.
            asteroids.remove(asteroid)

# We maken een aparte update loop voor ons ruimteschip.
def update_spaceship():
    # Als we spatiebalk indrukken schieten we een nieuw projectiel.
    if keyboard.space:
        if not spaceship.fired:
            spaceship.fired = True
            new_projectile = Actor('projectile')
            new_projectile.x = spaceship.x
            new_projectile.y = spaceship.y - 30
            spaceship.projectiles.append(new_projectile)
            sounds.laser.play()
    else:
        spaceship.fired = False

    # Als we het pijltje naar rechts indrukken en we worden niet tegengehouden:
    if keyboard.right and not spaceship.BLOCKED_RIGHT:
        # Dan kunnen we weer naar links bewegen.
        spaceship.BLOCKED_LEFT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed < spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed += spaceship.X_ACCELERATION
    # Als we het pijltje naar links indrukken en we worden niet tegengehouden:
    elif keyboard.left and not spaceship.BLOCKED_LEFT:
        # Dan kunnen we weer naar rechts bewegen.
        spaceship.BLOCKED_RIGHT = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.x_speed > -spaceship.MAX_X_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.x_speed -= spaceship.X_ACCELERATION
    # Als we niet rechts of links indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.x_speed < -1:
            spaceship.x_speed += spaceship.AIR_RESISTANCE
        elif spaceship.x_speed > 1:
            spaceship.x_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.x_speed = 0

    # Als het ruimteschip tegen de rechterkant van het scherm aankomt:
    if spaceship.x > WIDTH:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar rechts gaan.
        spaceship.x = WIDTH
        spaceship.x_speed = 0
        spaceship.BLOCKED_RIGHT = True
    # Als het ruimteschip tegen de linkerkant van het scherm aankomt:
    elif spaceship.x < 0:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar links gaan.
        spaceship.x = 0
        spaceship.x_speed = 0
        spaceship.BLOCKED_LEFT = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.x += spaceship.x_speed

    # Als we pijltje omhoog indrukken en we worden niet tegengehouden:
    if keyboard.up and not spaceship.BLOCKED_UP:
        # Dan kunnen we weer naar onderen bewegen.
        spaceship.BLOCKED_DOWN = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed < spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
           spaceship.y_speed += spaceship.Y_ACCELERATION
    # Als we pijltje naar beneden indrukken en we worden niet tegengehouden:
    elif keyboard.down and not spaceship.BLOCKED_DOWN:
        # Dan kunnen we weer naar boven bewegen.
        spaceship.BLOCKED_UP = False
        # En als we nog niet de maximale snelheid bereikt hebben:
        if spaceship.y_speed > -spaceship.MAX_Y_SPEED:
            # Dan versnellen we ons ruimteschip.
            spaceship.y_speed -= spaceship.Y_ACCELERATION
    # Als we niet naar boven of naar onderen indrukken
    else:
        # Dan remmen we het ruimteschip af met behulp van de AIR_RESISTANCE.
        if spaceship.y_speed < -1:
            spaceship.y_speed += spaceship.AIR_RESISTANCE
        elif spaceship.y_speed > 1:
            spaceship.y_speed -= spaceship.AIR_RESISTANCE
        else:
            spaceship.y_speed = 0

    # Als het ruimteschip tegen de onderkant van het scherm aankomt:
    if spaceship.y > HEIGHT:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar beneden gaan.
        spaceship.y = HEIGHT
        spaceship.y_speed = 0
        spaceship.BLOCKED_DOWN = True
    # Als het ruimteschip tegen de bovenkant van het speelveld aankomt (we nemen niet het hele scherm):
    elif spaceship.y < 400:
        # Dan stoppen we het ruimteschip daar en blokkeren we naar boven gaan.
        spaceship.y = 400
        spaceship.y_speed = 0
        spaceship.BLOCKED_UP = True
    # Anders dan verrekenen we de snelheid van het ruimteschip met de snelheid van het ruimteschip.
    else:
        spaceship.y -= spaceship.y_speed


def update_backgound():
    background.y += background.speed

    if background.y > 1000:
        background.y = 0

def update_projectiles():
    # Voor elke projectiel in de lijst van projectielen:
    for projectile in spaceship.projectiles:
        # Beweeg het projectiel vooruit.
        projectile.y -= spaceship.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if projectile.y < 0:
            # Dan halen we het projectiel uit de lijst.
            spaceship.projectiles.remove(projectile)


def check_for_collisions():
    global asteroids, game_over, score, ASTEROID_SPAWN_RATE, ASTEROID_SPEED
    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in spaceship.projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        spaceship.projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        ASTEROID_SPEED += 0.25
                        if ASTEROID_SPAWN_RATE > 0.25:
                            ASTEROID_SPAWN_RATE -= 0.25


def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        update_spaceship()
        update_backgound()
        update_asteroids()
        update_projectiles()
        check_for_collisions()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()
    for asteroid in asteroids:
        asteroid.draw()

    for projectile in spaceship.projectiles:
        projectile.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

# Start het spwawnen van asteroïden
spawn_asteroids()
```
We hebben nu een begin van een spel. Dit gaan we in het volgende hoofdstuk verder uitbreiden met een menu en we gaan highscores bijhouden. Dus ga door naar de volgende opdracht of probeer de extra uitdaging.

[<- Terug](6-botsingen.md) - [Volgende ->](8-tyrian-verbeteringen.md)

##### Extra uitdaging
Voeg een toets toe om makkelijk het spel te kunnen herstarten nadat je game over ging. Kijk voor hoe je dat zou kunnen doen even terug naar de vorige opdracht.

<details><summary>Hint</summary>
Maak een functie "restart_game()" deze roep je dan aan zodra "game_over" waar is en je op "r" drukt. In deze functie zet je alle variablen terug die invloed hebben op het spel verloop naar hun originele waarde.
</details>

#### Credits
De achter grond is gemaakt door [Marja]( https://www.instagram.com/marjavdhoning/). Het plaatje voor het ruimteschip, het projectiel en de explosie zijn gemaakt door [CDmir](https://opengameart.org/users/cdmir) en komen uit zijn [asteroids-game-sprites-atlas](https://opengameart.org/content/asteroids-game-sprites-atlas). Het geluid voor de laser komen van [laser-fire](https://opengameart.org/content/laser-fire) en zijn gemaakt door [dklon](https://opengameart.org/users/dklon). Het geluid voor de explosie komt van [explosion-0](https://opengameart.org/content/explosion-0) en is gemaakt door [TinyWorlds](https://opengameart.org/users/tinyworlds).
