from pgzero.builtins import Actor

class Projectile(Actor):
    def __init__(self, image):
        Actor.__init__(self, image)
        self.PROJECTILE_SPEED = 16

    def update(self, projectiles):
        # Beweeg het projectiel voor uit.
        self.y -= self.PROJECTILE_SPEED
        # Als het projectiel van het scherm af is:
        if self.y < 0:
            # Dan halen we het projectiel uit de lijst.
            projectiles.remove(self)