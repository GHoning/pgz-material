## Actoren
Je kunt het speelveld van een game zien als een toneel waar zich het spel afspeelt en acteurs als de objecten en de speler. Om dit in te richten gebruikt Pygame-Zero wat zij Actors noemen. In Pygame-Zero hebben Actors veel verschillende eigenschappen, maar wij gaan eerst alleen aan de slag met het uiterlijk en de positie van een Actor.

### Achtergrond
Laten we beginnen met een achtergrond voor onze game. Om een plaatje toe te voegen aan ons project kunnen we op de knop ```Images``` klikken. Dit opent een venster waar je, je plaatjes kunt neer zetten. Op deze manier weet Pygame-Zero waar je plaatjes staan.

Download de achtergrond [hier](2-actoren/background.png) en zit hem in de images folder. Maak een nieuw bestand aan en noem die ```actors.py``` en zet daar de volgende code in.

```python
WIDTH = 640
HEIGHT = 480

#Hier maken we onze actor aan. We gebruiken de actor nu als achtergrond.
background = Actor('background')

#Hier zeggen we dat de actor getekend moet worden door de computer.
def draw():
    background.draw()
```

### Karakter
Nu willen we een natuurlijk ook een acteur op het podium hebben deze kun je [hier](2-actoren/player.png) downloaden. Voeg de volgende code toe aan je ```actors.py``` om de speler toe te voegen aan het spelveld. Je ziet dat we twee getalen hebben toegevoegd voor de speler. Dit zijn de coördinaten waar de speler getekend wordt. Hierover meer in het volgende hoofdstuk.

```python
WIDTH = 640
HEIGHT = 480

background = Actor('background')
# We maken een speler aan, net als we gedaan hebben voor de achtergrond.
player = Actor('player', (340, 240))

def draw():
    background.draw()
    player.draw()
```

Je weet nu hoe je Actoren met een plaatje toevoegt aan het speelveld.

[<- Terug](1-tekenen.md) - [Volgende ->](3-bewegen.md)

##### Extra uitdaging
Zoek zelf een cool plaatje op en voeg die toe aan het speelveld. Door de zelfde stappen te volgen als je ook gedaan hebt voor de achtergrond en het karakter.

#### Credits
Alle plaatjes zijn gemaakt door [Kenney](https://kenney.nl).
