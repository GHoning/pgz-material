WIDTH = 1024
HEIGHT = 1024

background = Actor('background_move')
alien2 = Actor('alien2', (512, 512))
# We maken een extra variable aan op de alien om bij te houden of er gesleept wordt met de alien.
alien2.dragging = False

def draw():
    background.draw()
    alien2.draw()

# Hier gebruiken we de eerste muis gerelateerde functie van Pygame-Zero
def on_mouse_down(pos):
  # We controleren of de muis klik plaats op onze alien.
  if alien2.collidepoint(pos):
    # Als dat zo is beginnen we met het slepen van de alien.
    alien2.dragging = True

# Zodra we door krijgen dat de muisknop niet meer indrukt is
def on_mouse_up():
  # Stoppen we met het slepen van de alien.
  alien2.dragging = False

# Hier krijgen we door of de muis cursor zich verplaatst.
def on_mouse_move(pos):
  # We controleren of we de alien aan het slepen zijn.
  if alien2.dragging:
    # Als dat het geval is zetten we de positie van de alien gelijk aan die van de muis cursor.
    alien2.pos = pos