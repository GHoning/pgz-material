import random
from spaceship import Spaceship
from asteroid import Asteroid
from scrolling_background import ScrollingBackground
from pickup import Pickup

# Game variablen.
TITLE = 'tyrian'
WIDTH = 900
HEIGHT = 1000
game_over = False
score = 0

# Ons ruimteschip met al zijn variablen.
spaceship = Spaceship('spaceship', WIDTH, HEIGHT)

# Onze achtergronden.
background = ScrollingBackground('space_background')

# Asteroïden
asteroid_spawn_rate = 2
asteroid_speed = 2
asteroids = []

projectiles = []

pickups = []
pickup_spawn_rate = 0.8

def spawn_asteroids():
    global asteroids
    # Als de game nog niet voorbij is:
    if not game_over:
        # Dan spawnen we een nieuwe asteroïde in.
        new_asteroid = Asteroid('asteroid', HEIGHT)
        new_asteroid.x = random.randint(0, WIDTH)
        new_asteroid.y = 0
        new_asteroid.speed = asteroid_speed
        # We voegen de nieuwe asteroïde aan de lijst van asteroïden toe.
        asteroids.append(new_asteroid)
        # We zetten een timer voor het spawnen van een nieuwe asteroïde.
        clock.schedule_unique(spawn_asteroids, asteroid_spawn_rate)


def check_for_collisions():
    global projectiles, asteroids, pickups, game_over, score, asteroid_spawn_rate, asteroid_speed
    for pickup in pickups:
        if pickup is not None:
            if pickup.colliderect(spaceship):
                pickups.remove(pickup)
                sounds.pickup.play()
                score += 5

    # Voor asteroïde in asteroïden
    for asteroid in asteroids:
        if asteroid is not None:
            # Als asteroïde het ruimteschip raakt:
            if asteroid.colliderect(spaceship):
                # Is het spel over en explodeert het ruimteschip.
                game_over = True
                sounds.explosion.play()
                spaceship.image = "explosion"

            # Voor projectiel in de lijst van projectielen.
            for projectile in projectiles:
                if projectile is not None:
                    # Als asteroïde geraakt wordt door het projectiel.
                    if asteroid.colliderect(projectile):
                        # Spelen we een geluid af
                        sounds.explosion.play()
                        # Halen we de asteroïde en het projectiel uit de lijst.
                        asteroids.remove(asteroid)
                        projectiles.remove(projectile)
                        # Tellen we een punt op bij de score en zorgen we dat de asteroïden sneller gaan en spawnen.
                        score += 1
                        asteroid_speed += 0.25
                        if asteroid_spawn_rate > 0.25:
                            asteroid_spawn_rate -= 0.25
                        if random.random() > pickup_spawn_rate:
                            new_pickup = Pickup("pickup", HEIGHT)
                            new_pickup.x = asteroid.x
                            new_pickup.y = asteroid.y
                            new_pickup.speed = 2
                            pickups.append(new_pickup)

def restart_game():
    global projectiles, asteroids, game_over, score, asteroid_spawn_rate, asteroid_speed
    score = 0
    asteroids = []
    projectiles = []
    game_over = False
    asteroid_spawn_rate = 2
    asteroid_speed = 2
    spaceship.image = "spaceship"
    spawn_asteroids()

def update():
    # Als de game nog niet voorbij is updaten we ons ruimteschip en andere spel elementen.
    if not game_over:
        background.update()
        spaceship.update(projectiles)

        for projectile in projectiles:
            projectile.update(projectiles)

        for asteroid in asteroids:
            asteroid.update(asteroids)

        for pickup in pickups:
            pickup.update(pickups)

        check_for_collisions()
    elif keyboard.r and game_over:
        restart_game()

def draw():
    # We tekenen de achtergrond en het ruimteschip.
    screen.clear()
    background.draw()
    spaceship.draw()

    for asteroid in asteroids:
        asteroid.draw()

    for projectile in projectiles:
        projectile.draw()

    for pickup in pickups:
        pickup.draw()

    screen.draw.text(
        str(score),
        color='white',
        midtop =(WIDTH/2, 10),
        fontsize=70,
    )

    if game_over:
        screen.draw.text(
            "Game Over\nPress \"R\" to restart",
            color='white',
            midtop =(WIDTH/2, HEIGHT/2),
            fontsize=70,
        )

# Start het spawnen van asteroïden
spawn_asteroids()
