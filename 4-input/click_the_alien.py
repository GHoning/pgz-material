WIDTH = 1024
HEIGHT = 1024

background = Actor('background_move')
alien2 = Actor('alien2', (512, 512))
alien2.clicked = False

def draw():
    background.draw()
    alien2.draw()

def update():

  if keyboard.right:
    alien2.x += 2

  if keyboard.left:
    alien2.x -= 2

  if keyboard.up:
    alien2.y -= 2

  if keyboard.down:
    alien2.vy += 2

def on_mouse_down(pos):
    if alien2.collidepoint(pos):
        if alien2.clicked:
            alien2.clicked = False
            alien2.image = 'alien2_hit'
        else:
            alien2.clicked = True
            alien2.image = 'alien2'