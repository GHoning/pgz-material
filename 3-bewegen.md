## Bewegen
Het is gaaf dat we nu met behulp van actors plaatjes in ons speelveld kunnen zetten, maar het zou nog cooler zijn als we de actoren kunnen bewegen. Om dit te doen gaan we gaan we gebruik maken van de ```update``` functie van Pygame-Zero en de coördinaten van de actoren. De ```update``` functie wordt elke frame van onze game aangeroepen dit betekend dat de inhoud van deze functie ieder frame herhaald wordt.

### Alien 1
We gaan een actor verpaatsen over het speelveld, hiervoor gebruiken we de ```update``` functie. Deze functie kun je gebruiken om allerlei dingen te veranderen op je speelveld zoals de coördinaten van een actor. Download de plaatjes die je nodig hebt de [alien1](3-bewegen/alien1.png) en de [achtergrond](3-bewegen/background_race.png). Open een nieuw bestand in Mu en noem die ```alien1.py```.

```python
WIDTH = 1648
HEIGHT = 450

background = Actor('background_race')
# We beginnen deze keer bij 0, 480 zodat we genoeg ruimte hebben om op te bewegen.
alien1 = Actor('alien1', (0, 480))

def draw():
    background.draw()
    alien1.draw()

# De update functie kun je gebruiken om van alles te updaten op je speelveld zoals de positie van onze alien.
def update():
  # We tellen 2 op bij het x-coördinaat, hierdoor gaat de alien naar rechts.
  alien1.x = alien1.x + 2
  # We halen 1 af van het y-coördinaat, hierdoor gaat de alien naar boven.
  alien1.y = alien1.y - 1
```

### Richtingen
Om de alien te bewegen gebruiken we het x en y coördinaat van de actor. Door een getal van y af te halen gaat de alien naar boven, door er een getal bij op te tellen gaat de alien naar beneden. Door een getal bij de x op te tellen gaat de alien naar rechts, door er een getal af te trekken gaat de alien naar links.

![richtingen](3-bewegen/directions.png)

### Alien 2
Een bewegende alien is al beter dan een stilstaande, maar we kunnen hem ook steeds sneller laten gaan zoals een auto die optrekt. Voor je begint download [alien2](3-bewegen/alien2.png). Open een nieuw bestand in Mu en noem die ```alien2.py```.

```python
WIDTH = 1648
HEIGHT = 450

background = Actor('background_race')
alien2 = Actor('alien2', (0, 225))
alien2.vx = 0
alien2.vy = 0

def draw():
  background.draw()
  alien2.draw()

def update():
  # Dit is een afkorting voor alien2.vx = alien2.vx  + 0.5
  alien2.vx += 0.5
  alien2.vy += 0.05

  alien2.x += alien2.vx
  alien2.y += alien2.vy
```

### Versnelling
In het eerste voorbeeld tellen we een getal op bij het x en y coördinaat van de alien. Zoals dit:
```python
x += 1
y += 2
```
In het tweede voorbeeld gebruiken we wat we een tweede variable. Zoals het volgende:
```python
vx += 1
vy += 2

x += vx
y += vy
```
Dus in plaats van direct het x en y coördinaat van de alien aan te passen doen we de aanpassing op een tweede variable en deze tellen we dan op bij het x en y coördinaat. Dit zorgt er voor dat het getal dat we optellen bij het x en y coördinaat elke update steeds groter wordt. Hierdoor gaat hij dus steeds sneller.

Je weet nu hoe je actoren kunt bewegen, probeer de extra uitdaging of ga door naar de volgende opdracht.

[<- Terug](2-actoren.md) - [Volgende ->](4-input.md)

##### Extra uitdaging
Probeer om een de aliens tegen elkaar te laten racen door de projecten samen te voegen. Je kunt dit makkelijker maken door alleen te racen over de x-as.

<details><summary>Hint</summary>
Probeer om de draw en update functies samen te voegen.

```python
def draw():
  background.draw()
  alien1.draw()
  alien2.draw()

def update():
  alien1.x += 2
  alien2.vx += 0.5
  alien2.x += alien2.vx
```
</details>

#### Credits
Alle plaatjes zijn gemaakt door [Kenney](https://kenney.nl).
