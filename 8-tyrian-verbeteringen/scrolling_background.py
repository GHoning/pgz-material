from pgzero.builtins import Actor

class ScrollingBackground():
    def __init__(self, image):
        self.background = Actor(image)
        self.background.y = 0
        self.speed = 2

    def update(self):
        self.background.y += self.speed

        if self.background.y > 1000:
            self.background.y = 0
            
    def draw(self):
        self.background.draw()