from pgzero.builtins import Actor

class Pickup(Actor):
    def __init__(self, image, maxHeight):
        Actor.__init__(self, image)
        self.speed = 2
        self.maxHeight = maxHeight

    def update(self, pickups):
        self.y += self.speed
        # Als de pickup van het veld af is:
        if self.y > self.maxHeight:
            # Verwijderen we de pickup.
            pickups.remove(self)
