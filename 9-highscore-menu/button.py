from pgzero.rect import Rect
import pygame.mouse as mouse

# De button klasse
class Button():
    # De variabelen van de de button klasse.
    box = None
    color = None
    name = None
    textPos = None
    function = None

    def __init__(self, name, pos, size, color, function, **kwargs):
        self.name = name
        self.box = Rect(pos, size)
        self.color = color
        self.textPos = (pos[0] + size[0]/2, pos[1] + size[1]/2)
        # We gaan eens iets nieuws doen, we geven een functie mee.
        self.function = function

    def draw(self, screen):
        screen.draw.filled_rect(self.box, self.color)
        screen.draw.text(
            str(self.name),
            color='white',
            center=self.textPos,
            fontsize=35,
            shadow=(1, 1)
        )

    def update(self):
        # Als linkermuisknop ingedrukt wordt en de muis cursor bevindt zich binnen de knop voer de knop functie uit.
        if mouse.get_pressed()[0] and self.box.collidepoint(mouse.get_pos()):
            self.function()
