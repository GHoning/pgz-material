WIDTH = 1648
HEIGHT = 450
PLAYER_START_POS = (200, 325)
GRAVITY = 0.3

background = Actor('background_race')
alien = Actor('alien_right', PLAYER_START_POS)
alien.vx = 0
alien.vy = 0
alien.jumped = False

coin = Actor('coin', (1400, 368))
coin.picked_up = False

gameover = False
spikes = Actor('spikes', (700, 350))
score = 0

def draw():
  background.draw()
  if not coin.picked_up:
    coin.draw()

  spikes.draw()
  alien.draw()

  screen.draw.text(
    str(score),
    color='white',
    midtop=(WIDTH // 2, 10),
    fontsize=70,
    shadow=(1, 1)
  )

  if gameover:
    screen.draw.text(
      "Game Over\nPress \"R\" to restart",
      color='white',
      midtop=(WIDTH // 2, HEIGHT // 2),
      fontsize=70,
      shadow=(1, 1)
    )

def update():
  global gameover

  if not gameover:
  # Bereken X beweging
    if keyboard.right:
      alien.image = "alien_right"
      alien.vx += 0.3
    elif keyboard.left:
      alien.image = "alien_left"
      alien.vx -= 0.3
    else:
      alien.vx = 0

    if alien.vx > 4:
      alien.vx = 4

    if alien.vx < -4:
      alien.vx = -4

    alien.x += alien.vx

    # Bereken Y beweging
    if keyboard.up and not alien.jumped:
      alien.vy -= 10
      alien.jumped = True
    elif alien.jumped:
      alien.vy += GRAVITY

    if alien.jumped:
      alien.y += alien.vy

    if alien.y >= 325:
      alien.y = 325
      alien.jumped = False

    # Pickup Coin
    if alien.colliderect(coin) and not coin.picked_up:
      global score
      score += 10
      coin.picked_up = True
      sounds.coin.play()

    # Hitspikes
    if alien.colliderect(spikes):
      sounds.eep.play()
      gameover = True

  # Respawn player
  if keyboard.r:
    global score
    gameover = False
    alien.pos = PLAYER_START_POS
    score = 0
