## Geluiden
Nu gaan we een soundboard maken om te leren hoe we geluiden afspelen in Pygame-zero. Om dit te bouwen gaan we gebruik maken van lijsten, en de ```souds.play()``` functie van Pygame-zero.

### Geluiden toevoegen en afspelen
Laten we beginnen met het afspelen van een geluid. Dit doen we door op de knop ```sounds``` te klikken. Hier krijg je dan een folder te zien. Hier staat als het goed is al een geluid in, namelijk ```eep.wav```. Deze gaan we afspelen met dit stukje code.
```python
# het middelste woord is de naam van het geluid dat je wilt afspelen
sounds.eep.play()
```

Probeer het maar eens uit in een nieuw bestand. Nu we dit weten kunnen we een soundboard maken.

### Soundboard
In het soundboard gaan we 4 knoppen maken, die 4 verschillende geluiden gaan afspelen. De bestandend die je daar voor nodig hebt kun je hier vinden:

* Koe [plaatje](5-geluiden/images/cow.png) en [geluid](5-geluiden/sounds/cow.ogg)
* Eend [plaatje](5-geluiden/images/duck.png) en [geluid](5-geluiden/sounds/duck.ogg)
* Varken [plaatje](5-geluiden/images/pig.png) en [geluid](5-geluiden/sounds/pig.ogg)
* Schaap [plaatje](5-geluiden/images/sheep.png) en [geluid](5-geluiden/sounds/sheep.ogg)

Maak een nieuw bestand aan en noem dit ```soundboard.py``` en zet de volgende code er om je soundboard te maken.

```python
WIDTH = 330
HEIGHT = 290

# We creëren de knoppen voor ons soundboard
cow = Actor('cow', (85, 75))
duck = Actor('duck', (245, 75))
pig = Actor('pig', (85, 215))
sheep = Actor('sheep', (245, 215))

# We tekenen de knoppen
def draw():
    cow.draw()
    duck.draw()
    pig.draw()
    sheep.draw()

# Hier checken we of er op de knoppen geklikt is en zo ja spelen we het geluid af
def on_mouse_down(pos):
  if cow.collidepoint(pos):
    sounds.cow.play()
  elif duck.collidepoint(pos):
    sounds.duck.play()
  elif pig.collidepoint(pos):
    sounds.pig.play()
  elif sheep.collidepoint(pos):
    sounds.sheep.play()
```

Je weet nu hoe je geluiden kunt afspelen, probeer de extra uitdaging of ga door naar de volgende opdracht.

[<- Terug](4-input.md) - [Volgende ->](6-botsingen.md)

##### Extra uitdaging
Probeer je eigen geluiden toe te voegen aan het soundboard door zelf een plaatje en een geluid op te zoeken dat je kunt gebruiken.

#### Credits
Alle plaatjes zijn gemaakt door [Marja]( https://www.instagram.com/marjavdhoning/). De geluiden komen van [Mudchute Park and Farm](http://commons.wikimedia.org/wiki/Category:Mudchute_Park_and_Farm) opgenomen door [Secretlondon](http://commons.wikimedia.org/wiki/User:Secretlondon)
