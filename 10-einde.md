## Einde
Gefeliciteerd met het bereiken van het einde, maar wat nu?

### Doe de uitdagingen
Onderaan elk hoofdstuk staat een uitdaging.
De antwoorden voor de code uitdagingen staan in hun desbetreffende folder hieronder gelinkt.

[0-installatie](0-installatie/)  
[1-tekenen](1-tekenen/)  
[2-actoren](2-actoren/)  
[3-bewegen](3-bewegen/)  
[4-input](4-input/)  
[5-geluiden](5-geluiden/)  
[6-botsingen](6-botsingen/)  
[7-tyrian](7-tyrian/)  
[8-tyrian-verbeteringen](8-tyrian-verbeteringen/)  
[9-highscore-menu](9-highscore-menu/)  

### Bouw je eigen game
Probeer je eigen game te bouwen met de kennis die je nu op hebt gedaan. Voor plaatjes en geluiden voor je spel kun je kijken op [OpenGameArt](https://opengameart.org/). Bedenk eerst hoe je spel moet gaan werken. Wat kan de speler allemaal doen? Hoe verplaatst hij/zij zich door de wereld? Wat voor obstakels zijn er voor de speler om te overwinnen? Voor meer inspiratie kun je eens kijken in de voorbeelden van Pygame-Zero zelf.

### Links voor meer informatie
[Pygame-Zero documentatie](https://pygame-zero.readthedocs.io/en/stable/)  
[Mu-editor tutorials](https://codewith.mu/en/tutorials/)  
[Mu-editor how-to Guides](https://codewith.mu/en/howto/)  
[Pygame-Zero voorbeeld games](https://github.com/lordmauve/pgzero/tree/master/examples)

Mocht je helemaal klaar zijn met pygame zero of wordt wat je wilt bouwen wat te groot voor pygame zero. Kijk dan eens naar [Godot](https://godotengine.org/) dit is een game engine waarvan het ```GDscript``` erg lijkt op python. Om daarmee te starten kun je bij hun [step by step](https://docs.godotengine.org/en/stable/getting_started/step_by_step/index.html) beginnen, dit is wel allemaal in het Engels.


[<- Terug](9-highscore-menu.md)